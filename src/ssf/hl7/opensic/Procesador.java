package ssf.hl7.opensic;

import java.sql.SQLException;
import java.text.ParseException;

import sqlj.runtime.ref.DefaultContext;
import ssf.hl7.Debug;
import ssf.hl7.msg.Msg;

public interface Procesador {
  public int procesar(DefaultContext ctx, Msg m, int numEve, Debug dd) throws
      SQLException,
      PKAutoIncrementException,
      ParseException;
}