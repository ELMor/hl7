header{
	package ssf.hl7.parser;
	import  ssf.hl7.msg.*;
}

class hl7Parser extends Parser;
options {
	buildAST = false;
	k=10;
}
/* regla principal, devuelve un mensaje*/
msg returns [Msg m]
{
	m=null; //Inicializacion obligatoria
}
	:	 m=adta28
	|  m=adta31
	|  m=adta34
	|  m=orro02
	|  m=orur01
	|  m=dftp03
	;

/* Definicion de Mensajes que deben ser procesados a su recepcion
   No es necesario hacer parsing de los mensajes salientes,
   debido a que suponemos que los construimos bien
*/

adta28 returns [Msg m]
{
  m=new Msg();
  Segment s1,s2,s3,s4,s5;
}
	: s1=mshADTA28
		s2=evn
		s3=pid
		(ign!)?
		s4=pv1
		(ign!)*
		s5=in1
		EOF
	  {m.addSegment(s1);
	   m.addSegment(s2);
	   m.addSegment(s3);
	   m.addSegment(s4);
	   m.addSegment(s5);
	  }
	;

adta31 returns [Msg m]
{
  m=new Msg();
  Segment s1,s2,s3,s4,s5;
}
	: s1=mshADTA31
	  s2=evn
	  s3=pid
	  (ign!)?
	  s4=pv1
	  (ign!)*
	  s5=in1
	  (ign!)*
	  EOF
	  {m.addSegment(s1);
	   m.addSegment(s2);
	   m.addSegment(s3);
	   m.addSegment(s4);
	   m.addSegment(s5);
	  }
	;

adta34 returns [Msg m]
{
  m=new Msg();
  Segment s1,s2,s3,s4;
}
	: s1=mshADTA34
	  s2=evn
	  s3=pid
	  s4=mrg
	  EOF
	  {m.addSegment(s1);
	   m.addSegment(s2);
	   m.addSegment(s3);
	   m.addSegment(s4);
	  }
	;

orro02 returns [Msg m]
{
  m=new Msg();
  Segment s1,s2,s3;
}
	: s1=mshORRO02 	{m.addSegment(s1);}
	  (ign!)?
	  s2=pid				{m.addSegment(s2);}
	  s3=pv1				{m.addSegment(s3);}
	  lOrcObr[m]
	  EOF
	;

lOrcObr [Msg m]
{
	Segment s1,s2;
}
	: (s1=orc {m.addSegment(s1);} s2=obr {m.addSegment(s2);} )+
	;

orur01 returns [Msg m]
{
  m=new Msg();
  Segment s1,s2,s3;
}
	: s1=mshORUR01		{m.addSegment(s1);}
	  (ign!)?					//Ignorar si hay segmentos
	  s2=pid 				 	{m.addSegment(s2);}
	  (ign!)?					//Ignorar si hay segmentos
	  s3=pv1 					{m.addSegment(s3);}
	  lOrcltObrObx[m] //Trato especial, ver regla
	  EOF
	;

tObrObx[Msg m]
{
	Segment s1,s2;
}
	: s1=obr {m.addSegment(s1);} (s2=obx {m.addSegment(s2);}  )+
	;

ltObrObx[Msg m]
	: (tObrObx[m])+
	;

lOrcltObrObx [Msg m]
{
	Segment s1,s2;
}
	: (s1=orc {m.addSegment(s1);} ltObrObx[m])+
	;

dftp03 returns [Msg m]
{
  m=new Msg();
  Segment s1,s2,s3,s4,s5;
}
	: s1=mshDFTP03 	{m.addSegment(s1);}
		s2=evn 			 	{m.addSegment(s2);}
		s3=pid 			 	{m.addSegment(s3);}
		s4=pv1 			 	{m.addSegment(s4);}
		(s5=ft1{m.addSegment(s5);})+
		EOF
	;

/*  Fin definicion de mensajes */

/* definicion Segmentos */
mshADTA28	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MSH");
  s.addField(new Field(null,"^~\\&") );
}
	: MSH
				(f=field {s.addField(f); })+
		     ADTA28  {s.addField(new Field(null,"ADT^A28") );}
		    (f=field {s.addField(f); })+
		    ENDSEG
	;

mshADTA31	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MSH");
}
	: MSH
				(f=field {s.addField(f); })+
		     ADTA31  {s.addField(new Field(null,"ADT^A31") );}
		    (f=field {s.addField(f); })+
		    ENDSEG
;

mshADTA34	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MSH");
}
	: MSH
				(f=field {s.addField(f); })+
		     ADTA34  {s.addField(new Field(null,"ADT^A34") );}
		    (f=field {s.addField(f); })+
		    ENDSEG
	;

mshORRO02	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MSH");
}
	: MSH
				(f=field {s.addField(f); })+
		     ORRO02  {s.addField(new Field(null,"ORR^O02") );}
		    (f=field {s.addField(f); })+
		    ENDSEG
	;

mshORUR01	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MSH");
}
	: MSH
				(f=field {s.addField(f); })+
		     ORUR01  {s.addField(new Field(null,"ORU^R01") );}
		    (f=field {s.addField(f); })+
		    ENDSEG
	;

mshDFTP03	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MSH");
}
	: MSH
				(f=field {s.addField(f); })+
		     DFTP03  {s.addField(new Field(null,"DFT^P03") );}
		    (f=field {s.addField(f); })+
		    ENDSEG
	;

evn	returns [Segment s]
{
	Field f;
	s=new Segment(null,"EVN");
}
	: EVN 			(f=field {s.addField(f); })+ ENDSEG
	;

pid	returns [Segment s]
{
	Field f;
	s=new Segment(null,"PID");
}
	: PID 			(f=field {s.addField(f); })+ ENDSEG
	;

pv1	returns [Segment s]
{
	Field f;
	s=new Segment(null,"PV1");
}
	: PV1 			(f=field {s.addField(f); })+ ENDSEG
	;

in1	returns [Segment s]
{
	Field f;
	s=new Segment(null,"IN1");
}
	: IN1 			(f=field {s.addField(f); })+ ENDSEG
	;

mrg	returns [Segment s]
{
	Field f;
	s=new Segment(null,"MRG");
}
	: MRG 			(f=field {s.addField(f); })+ ENDSEG
	;

orc	returns [Segment s]
{
	Field f;
	s=new Segment(null,"ORC");
}
	: ORC 			(f=field {s.addField(f); })+ ENDSEG
	;

obr	returns [Segment s]
{
	Field f;
	s=new Segment(null,"OBR");
}
	: OBR 			(f=field {s.addField(f); })+ ENDSEG
	;

obx	returns [Segment s]
{
	Field f;
	s=new Segment(null,"OBX");
}
	: OBX 			(f=field {s.addField(f); })+ ENDSEG
	;

ft1	returns [Segment s]
{
	Field f;
	s=new Segment(null,"FT1");
}
	: FT1 			(f=field {s.addField(f); })+ ENDSEG	;

ign
	: CONTENT (field)+ ENDSEG	;

/********************** Fin definicion de Segmentos**********************/

field returns [Field f]
{
  f=null;
}
	: BEGINFIELD! {f=new Field(null,"");}
	  (f=field_content
	  	(BEGINREP! field_content)*
	  )?
	;

field_content returns [Field f]
{
	f=new Field(null,"");
	int i=1;
	Component c,c2;
}
	: c=component {f.putComponent(i++,c);}
		(
		 BEGINCOMP! {f.putComponent(i++,new Component(""));}
		 (  c2=component {f.putComponent(i-1,c2);}  )?
		)*
	;

component returns [ssf.hl7.msg.Component c]
{
  String s;
  c=null;
}
	: c1:CONTENT {s=c1.getText();} (BEGINSUBCOMP! c2:CONTENT {s=s+"^"+c2.getText();})*
		{ c=new Component(s); }
	;

 /* Definicion de Segmentos
 */


class hl7Lexer extends Lexer;
options {
	k=9;
	charVocabulary='\3'..'\377';
}

MSH: "MSH|^~\\&";
EVN: "EVN";
PID: "PID";
PV1: "PV1";
IN1: "IN1";
MRG: "MRG";
ORC: "ORC";
OBR: "OBR";
OBX: "OBX";
FT1: "FT1";

ADTA28:"|ADT^A28";
ADTA31:"|ADT^A31";
ADTA34:"|ADT^A34";
ORRO02:"|ORR^O02";
ORUR01:"|ORU^R01";
DFTP03:"|DFT^P03";

BEGINCOMP: 		'^' ;
BEGINREP: 		'~' ;
BEGINSUBCOMP: '&' ;
BEGINFIELD: 	'|' ;
ENDSEG: 			('\n' | "\r\n" | '\r' );

CONTENT
	: (~('^'|'~'|'&'|'|'|'\n' | '\r' ))+
	;

