// $ANTLR 2.7.1: "ssf/hl7/parser/hl7.g" -> "hl7Parser.java"$

	package ssf.hl7.parser;
	import  ssf.hl7.msg.*;

import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.collections.AST;
import antlr.ASTPair;
import antlr.collections.impl.ASTArray;

public class hl7Parser extends antlr.LLkParser
       implements hl7ParserTokenTypes
 {

protected hl7Parser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
}

public hl7Parser(TokenBuffer tokenBuf) {
  this(tokenBuf,10);
}

protected hl7Parser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
}

public hl7Parser(TokenStream lexer) {
  this(lexer,10);
}

public hl7Parser(ParserSharedInputState state) {
  super(state,10);
  tokenNames = _tokenNames;
}

	public final Msg  msg() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
			m=null; //Inicializacion obligatoria
		
		
		try {      // for error handling
			if ((LA(1)==MSH) && (LA(2)==BEGINFIELD) && (LA(3)==ADTA28||LA(3)==CONTENT||LA(3)==BEGINFIELD) && (_tokenSet_0.member(LA(4))) && (_tokenSet_1.member(LA(5))) && (_tokenSet_2.member(LA(6))) && (_tokenSet_2.member(LA(7))) && (_tokenSet_2.member(LA(8))) && (_tokenSet_3.member(LA(9))) && (_tokenSet_3.member(LA(10)))) {
				m=adta28();
			}
			else if ((LA(1)==MSH) && (LA(2)==BEGINFIELD) && (LA(3)==ADTA31||LA(3)==CONTENT||LA(3)==BEGINFIELD) && (_tokenSet_4.member(LA(4))) && (_tokenSet_5.member(LA(5))) && (_tokenSet_6.member(LA(6))) && (_tokenSet_6.member(LA(7))) && (_tokenSet_6.member(LA(8))) && (_tokenSet_7.member(LA(9))) && (_tokenSet_7.member(LA(10)))) {
				m=adta31();
			}
			else if ((LA(1)==MSH) && (LA(2)==BEGINFIELD) && (LA(3)==ADTA34||LA(3)==CONTENT||LA(3)==BEGINFIELD) && (_tokenSet_8.member(LA(4))) && (_tokenSet_9.member(LA(5))) && (_tokenSet_10.member(LA(6))) && (_tokenSet_10.member(LA(7))) && (_tokenSet_10.member(LA(8))) && (_tokenSet_11.member(LA(9))) && (_tokenSet_11.member(LA(10)))) {
				m=adta34();
			}
			else if ((LA(1)==MSH) && (LA(2)==BEGINFIELD) && (LA(3)==ORRO02||LA(3)==CONTENT||LA(3)==BEGINFIELD) && (_tokenSet_12.member(LA(4))) && (_tokenSet_13.member(LA(5))) && (_tokenSet_14.member(LA(6))) && (_tokenSet_14.member(LA(7))) && (_tokenSet_14.member(LA(8))) && (_tokenSet_15.member(LA(9))) && (_tokenSet_15.member(LA(10)))) {
				m=orro02();
			}
			else if ((LA(1)==MSH) && (LA(2)==BEGINFIELD) && (LA(3)==ORUR01||LA(3)==CONTENT||LA(3)==BEGINFIELD) && (_tokenSet_16.member(LA(4))) && (_tokenSet_17.member(LA(5))) && (_tokenSet_18.member(LA(6))) && (_tokenSet_18.member(LA(7))) && (_tokenSet_18.member(LA(8))) && (_tokenSet_19.member(LA(9))) && (_tokenSet_19.member(LA(10)))) {
				m=orur01();
			}
			else if ((LA(1)==MSH) && (LA(2)==BEGINFIELD) && (LA(3)==DFTP03||LA(3)==CONTENT||LA(3)==BEGINFIELD) && (_tokenSet_20.member(LA(4))) && (_tokenSet_21.member(LA(5))) && (_tokenSet_22.member(LA(6))) && (_tokenSet_22.member(LA(7))) && (_tokenSet_22.member(LA(8))) && (_tokenSet_23.member(LA(9))) && (_tokenSet_23.member(LA(10)))) {
				m=dftp03();
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Msg  adta28() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
		m=new Msg();
		Segment s1,s2,s3,s4,s5;
		
		
		try {      // for error handling
			s1=mshADTA28();
			s2=evn();
			s3=pid();
			{
			switch ( LA(1)) {
			case CONTENT:
			{
				ign();
				break;
			}
			case PV1:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			s4=pv1();
			{
			_loop5:
			do {
				if ((LA(1)==CONTENT)) {
					ign();
				}
				else {
					break _loop5;
				}
				
			} while (true);
			}
			s5=in1();
			match(Token.EOF_TYPE);
			m.addSegment(s1);
				   m.addSegment(s2);
				   m.addSegment(s3);
				   m.addSegment(s4);
				   m.addSegment(s5);
				
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Msg  adta31() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
		m=new Msg();
		Segment s1,s2,s3,s4,s5;
		
		
		try {      // for error handling
			s1=mshADTA31();
			s2=evn();
			s3=pid();
			{
			switch ( LA(1)) {
			case CONTENT:
			{
				ign();
				break;
			}
			case PV1:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			s4=pv1();
			{
			_loop9:
			do {
				if ((LA(1)==CONTENT)) {
					ign();
				}
				else {
					break _loop9;
				}
				
			} while (true);
			}
			s5=in1();
			{
			_loop11:
			do {
				if ((LA(1)==CONTENT)) {
					ign();
				}
				else {
					break _loop11;
				}
				
			} while (true);
			}
			match(Token.EOF_TYPE);
			m.addSegment(s1);
				   m.addSegment(s2);
				   m.addSegment(s3);
				   m.addSegment(s4);
				   m.addSegment(s5);
				
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Msg  adta34() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
		m=new Msg();
		Segment s1,s2,s3,s4;
		
		
		try {      // for error handling
			s1=mshADTA34();
			s2=evn();
			s3=pid();
			s4=mrg();
			match(Token.EOF_TYPE);
			m.addSegment(s1);
				   m.addSegment(s2);
				   m.addSegment(s3);
				   m.addSegment(s4);
				
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Msg  orro02() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
		m=new Msg();
		Segment s1,s2,s3;
		
		
		try {      // for error handling
			s1=mshORRO02();
			m.addSegment(s1);
			{
			switch ( LA(1)) {
			case CONTENT:
			{
				ign();
				break;
			}
			case PID:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			s2=pid();
			m.addSegment(s2);
			s3=pv1();
			m.addSegment(s3);
			lOrcObr(m);
			match(Token.EOF_TYPE);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Msg  orur01() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
		m=new Msg();
		Segment s1,s2,s3;
		
		
		try {      // for error handling
			s1=mshORUR01();
			m.addSegment(s1);
			{
			switch ( LA(1)) {
			case CONTENT:
			{
				ign();
				break;
			}
			case PID:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			s2=pid();
			m.addSegment(s2);
			{
			switch ( LA(1)) {
			case CONTENT:
			{
				ign();
				break;
			}
			case PV1:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			s3=pv1();
			m.addSegment(s3);
			lOrcltObrObx(m);
			match(Token.EOF_TYPE);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Msg  dftp03() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
		m=new Msg();
		Segment s1,s2,s3,s4,s5;
		
		
		try {      // for error handling
			s1=mshDFTP03();
			m.addSegment(s1);
			s2=evn();
			m.addSegment(s2);
			s3=pid();
			m.addSegment(s3);
			s4=pv1();
			m.addSegment(s4);
			{
			int _cnt32=0;
			_loop32:
			do {
				if ((LA(1)==FT1)) {
					s5=ft1();
					m.addSegment(s5);
				}
				else {
					if ( _cnt32>=1 ) { break _loop32; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt32++;
			} while (true);
			}
			match(Token.EOF_TYPE);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return m;
	}
	
	public final Segment  mshADTA28() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MSH");
		s.addField(new Field(null,"^~\\&") );
		
		
		try {      // for error handling
			match(MSH);
			{
			int _cnt35=0;
			_loop35:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt35>=1 ) { break _loop35; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt35++;
			} while (true);
			}
			match(ADTA28);
			s.addField(new Field(null,"ADT^A28") );
			{
			int _cnt37=0;
			_loop37:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt37>=1 ) { break _loop37; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt37++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_25);
		}
		return s;
	}
	
	public final Segment  evn() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"EVN");
		
		
		try {      // for error handling
			match(EVN);
			{
			int _cnt65=0;
			_loop65:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt65>=1 ) { break _loop65; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt65++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_26);
		}
		return s;
	}
	
	public final Segment  pid() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"PID");
		
		
		try {      // for error handling
			match(PID);
			{
			int _cnt68=0;
			_loop68:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt68>=1 ) { break _loop68; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt68++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_27);
		}
		return s;
	}
	
	public final void ign() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			match(CONTENT);
			{
			int _cnt92=0;
			_loop92:
			do {
				if ((LA(1)==BEGINFIELD)) {
					field();
				}
				else {
					if ( _cnt92>=1 ) { break _loop92; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt92++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_28);
		}
	}
	
	public final Segment  pv1() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"PV1");
		
		
		try {      // for error handling
			match(PV1);
			{
			int _cnt71=0;
			_loop71:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt71>=1 ) { break _loop71; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt71++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_29);
		}
		return s;
	}
	
	public final Segment  in1() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"IN1");
		
		
		try {      // for error handling
			match(IN1);
			{
			int _cnt74=0;
			_loop74:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt74>=1 ) { break _loop74; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt74++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_30);
		}
		return s;
	}
	
	public final Segment  mshADTA31() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MSH");
		
		
		try {      // for error handling
			match(MSH);
			{
			int _cnt40=0;
			_loop40:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt40>=1 ) { break _loop40; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt40++;
			} while (true);
			}
			match(ADTA31);
			s.addField(new Field(null,"ADT^A31") );
			{
			int _cnt42=0;
			_loop42:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt42>=1 ) { break _loop42; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt42++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_25);
		}
		return s;
	}
	
	public final Segment  mshADTA34() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MSH");
		
		
		try {      // for error handling
			match(MSH);
			{
			int _cnt45=0;
			_loop45:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt45>=1 ) { break _loop45; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt45++;
			} while (true);
			}
			match(ADTA34);
			s.addField(new Field(null,"ADT^A34") );
			{
			int _cnt47=0;
			_loop47:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt47>=1 ) { break _loop47; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt47++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_25);
		}
		return s;
	}
	
	public final Segment  mrg() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MRG");
		
		
		try {      // for error handling
			match(MRG);
			{
			int _cnt77=0;
			_loop77:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt77>=1 ) { break _loop77; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt77++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
		return s;
	}
	
	public final Segment  mshORRO02() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MSH");
		
		
		try {      // for error handling
			match(MSH);
			{
			int _cnt50=0;
			_loop50:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt50>=1 ) { break _loop50; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt50++;
			} while (true);
			}
			match(ORRO02);
			s.addField(new Field(null,"ORR^O02") );
			{
			int _cnt52=0;
			_loop52:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt52>=1 ) { break _loop52; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt52++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_31);
		}
		return s;
	}
	
	public final void lOrcObr(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			Segment s1,s2;
		
		
		try {      // for error handling
			{
			int _cnt17=0;
			_loop17:
			do {
				if ((LA(1)==ORC)) {
					s1=orc();
					m.addSegment(s1);
					s2=obr();
					m.addSegment(s2);
				}
				else {
					if ( _cnt17>=1 ) { break _loop17; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt17++;
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
	}
	
	public final Segment  orc() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"ORC");
		
		
		try {      // for error handling
			match(ORC);
			{
			int _cnt80=0;
			_loop80:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt80>=1 ) { break _loop80; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt80++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_32);
		}
		return s;
	}
	
	public final Segment  obr() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"OBR");
		
		
		try {      // for error handling
			match(OBR);
			{
			int _cnt83=0;
			_loop83:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt83>=1 ) { break _loop83; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt83++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_33);
		}
		return s;
	}
	
	public final Segment  mshORUR01() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MSH");
		
		
		try {      // for error handling
			match(MSH);
			{
			int _cnt55=0;
			_loop55:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt55>=1 ) { break _loop55; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt55++;
			} while (true);
			}
			match(ORUR01);
			s.addField(new Field(null,"ORU^R01") );
			{
			int _cnt57=0;
			_loop57:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt57>=1 ) { break _loop57; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt57++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_31);
		}
		return s;
	}
	
	public final void lOrcltObrObx(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			Segment s1,s2;
		
		
		try {      // for error handling
			{
			int _cnt29=0;
			_loop29:
			do {
				if ((LA(1)==ORC)) {
					s1=orc();
					m.addSegment(s1);
					ltObrObx(m);
				}
				else {
					if ( _cnt29>=1 ) { break _loop29; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt29++;
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_24);
		}
	}
	
	public final void tObrObx(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			Segment s1,s2;
		
		
		try {      // for error handling
			s1=obr();
			m.addSegment(s1);
			{
			int _cnt23=0;
			_loop23:
			do {
				if ((LA(1)==OBX)) {
					s2=obx();
					m.addSegment(s2);
				}
				else {
					if ( _cnt23>=1 ) { break _loop23; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt23++;
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_34);
		}
	}
	
	public final Segment  obx() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"OBX");
		
		
		try {      // for error handling
			match(OBX);
			{
			int _cnt86=0;
			_loop86:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt86>=1 ) { break _loop86; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt86++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_35);
		}
		return s;
	}
	
	public final void ltObrObx(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			{
			int _cnt26=0;
			_loop26:
			do {
				if ((LA(1)==OBR)) {
					tObrObx(m);
				}
				else {
					if ( _cnt26>=1 ) { break _loop26; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt26++;
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_36);
		}
	}
	
	public final Segment  mshDFTP03() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"MSH");
		
		
		try {      // for error handling
			match(MSH);
			{
			int _cnt60=0;
			_loop60:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt60>=1 ) { break _loop60; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt60++;
			} while (true);
			}
			match(DFTP03);
			s.addField(new Field(null,"DFT^P03") );
			{
			int _cnt62=0;
			_loop62:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt62>=1 ) { break _loop62; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt62++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_25);
		}
		return s;
	}
	
	public final Segment  ft1() throws RecognitionException, TokenStreamException {
		Segment s;
		
		
			Field f;
			s=new Segment(null,"FT1");
		
		
		try {      // for error handling
			match(FT1);
			{
			int _cnt89=0;
			_loop89:
			do {
				if ((LA(1)==BEGINFIELD)) {
					f=field();
					s.addField(f);
				}
				else {
					if ( _cnt89>=1 ) { break _loop89; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt89++;
			} while (true);
			}
			match(ENDSEG);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_37);
		}
		return s;
	}
	
/********************** Fin definicion de Segmentos**********************/
	public final Field  field() throws RecognitionException, TokenStreamException {
		Field f;
		
		
		f=null;
		
		
		try {      // for error handling
			match(BEGINFIELD);
			f=new Field(null,"");
			{
			switch ( LA(1)) {
			case CONTENT:
			{
				f=field_content();
				{
				_loop96:
				do {
					if ((LA(1)==BEGINREP)) {
						match(BEGINREP);
						field_content();
					}
					else {
						break _loop96;
					}
					
				} while (true);
				}
				break;
			}
			case ADTA28:
			case ENDSEG:
			case ADTA31:
			case ADTA34:
			case ORRO02:
			case ORUR01:
			case DFTP03:
			case BEGINFIELD:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_38);
		}
		return f;
	}
	
	public final Field  field_content() throws RecognitionException, TokenStreamException {
		Field f;
		
		
			f=new Field(null,"");
			int i=1;
			Component c,c2;
		
		
		try {      // for error handling
			c=component();
			f.putComponent(i++,c);
			{
			_loop100:
			do {
				if ((LA(1)==BEGINCOMP)) {
					match(BEGINCOMP);
					f.putComponent(i++,new Component(""));
					{
					switch ( LA(1)) {
					case CONTENT:
					{
						c2=component();
						f.putComponent(i-1,c2);
						break;
					}
					case ADTA28:
					case ENDSEG:
					case ADTA31:
					case ADTA34:
					case ORRO02:
					case ORUR01:
					case DFTP03:
					case BEGINFIELD:
					case BEGINREP:
					case BEGINCOMP:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
				}
				else {
					break _loop100;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_39);
		}
		return f;
	}
	
	public final ssf.hl7.msg.Component  component() throws RecognitionException, TokenStreamException {
		ssf.hl7.msg.Component c;
		
		Token  c1 = null;
		Token  c2 = null;
		
		String s;
		c=null;
		
		
		try {      // for error handling
			c1 = LT(1);
			match(CONTENT);
			s=c1.getText();
			{
			_loop103:
			do {
				if ((LA(1)==BEGINSUBCOMP)) {
					match(BEGINSUBCOMP);
					c2 = LT(1);
					match(CONTENT);
					s=s+"^"+c2.getText();
				}
				else {
					break _loop103;
				}
				
			} while (true);
			}
			c=new Component(s);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			consume();
			consumeUntil(_tokenSet_40);
		}
		return c;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"MSH",
		"ADTA28",
		"ENDSEG",
		"ADTA31",
		"ADTA34",
		"ORRO02",
		"ORUR01",
		"DFTP03",
		"EVN",
		"PID",
		"PV1",
		"IN1",
		"MRG",
		"ORC",
		"OBR",
		"OBX",
		"FT1",
		"CONTENT",
		"BEGINFIELD",
		"BEGINREP",
		"BEGINCOMP",
		"BEGINSUBCOMP"
	};
	
	private static final long _tokenSet_0_data_[] = { 65011744L, 0L };
	public static final BitSet _tokenSet_0 = new BitSet(_tokenSet_0_data_);
	private static final long _tokenSet_1_data_[] = { 65011808L, 0L };
	public static final BitSet _tokenSet_1 = new BitSet(_tokenSet_1_data_);
	private static final long _tokenSet_2_data_[] = { 65015904L, 0L };
	public static final BitSet _tokenSet_2 = new BitSet(_tokenSet_2_data_);
	private static final long _tokenSet_3_data_[] = { 65024096L, 0L };
	public static final BitSet _tokenSet_3 = new BitSet(_tokenSet_3_data_);
	private static final long _tokenSet_4_data_[] = { 65011840L, 0L };
	public static final BitSet _tokenSet_4 = new BitSet(_tokenSet_4_data_);
	private static final long _tokenSet_5_data_[] = { 65011904L, 0L };
	public static final BitSet _tokenSet_5 = new BitSet(_tokenSet_5_data_);
	private static final long _tokenSet_6_data_[] = { 65016000L, 0L };
	public static final BitSet _tokenSet_6 = new BitSet(_tokenSet_6_data_);
	private static final long _tokenSet_7_data_[] = { 65024192L, 0L };
	public static final BitSet _tokenSet_7 = new BitSet(_tokenSet_7_data_);
	private static final long _tokenSet_8_data_[] = { 65011968L, 0L };
	public static final BitSet _tokenSet_8 = new BitSet(_tokenSet_8_data_);
	private static final long _tokenSet_9_data_[] = { 65012032L, 0L };
	public static final BitSet _tokenSet_9 = new BitSet(_tokenSet_9_data_);
	private static final long _tokenSet_10_data_[] = { 65016128L, 0L };
	public static final BitSet _tokenSet_10 = new BitSet(_tokenSet_10_data_);
	private static final long _tokenSet_11_data_[] = { 65024320L, 0L };
	public static final BitSet _tokenSet_11 = new BitSet(_tokenSet_11_data_);
	private static final long _tokenSet_12_data_[] = { 65012224L, 0L };
	public static final BitSet _tokenSet_12 = new BitSet(_tokenSet_12_data_);
	private static final long _tokenSet_13_data_[] = { 65012288L, 0L };
	public static final BitSet _tokenSet_13 = new BitSet(_tokenSet_13_data_);
	private static final long _tokenSet_14_data_[] = { 65020480L, 0L };
	public static final BitSet _tokenSet_14 = new BitSet(_tokenSet_14_data_);
	private static final long _tokenSet_15_data_[] = { 65036864L, 0L };
	public static final BitSet _tokenSet_15 = new BitSet(_tokenSet_15_data_);
	private static final long _tokenSet_16_data_[] = { 65012736L, 0L };
	public static final BitSet _tokenSet_16 = new BitSet(_tokenSet_16_data_);
	private static final long _tokenSet_17_data_[] = { 65012800L, 0L };
	public static final BitSet _tokenSet_17 = new BitSet(_tokenSet_17_data_);
	private static final long _tokenSet_18_data_[] = { 65020992L, 0L };
	public static final BitSet _tokenSet_18 = new BitSet(_tokenSet_18_data_);
	private static final long _tokenSet_19_data_[] = { 65037376L, 0L };
	public static final BitSet _tokenSet_19 = new BitSet(_tokenSet_19_data_);
	private static final long _tokenSet_20_data_[] = { 65013760L, 0L };
	public static final BitSet _tokenSet_20 = new BitSet(_tokenSet_20_data_);
	private static final long _tokenSet_21_data_[] = { 65013824L, 0L };
	public static final BitSet _tokenSet_21 = new BitSet(_tokenSet_21_data_);
	private static final long _tokenSet_22_data_[] = { 65017920L, 0L };
	public static final BitSet _tokenSet_22 = new BitSet(_tokenSet_22_data_);
	private static final long _tokenSet_23_data_[] = { 65026112L, 0L };
	public static final BitSet _tokenSet_23 = new BitSet(_tokenSet_23_data_);
	private static final long _tokenSet_24_data_[] = { 2L, 0L };
	public static final BitSet _tokenSet_24 = new BitSet(_tokenSet_24_data_);
	private static final long _tokenSet_25_data_[] = { 4096L, 0L };
	public static final BitSet _tokenSet_25 = new BitSet(_tokenSet_25_data_);
	private static final long _tokenSet_26_data_[] = { 8192L, 0L };
	public static final BitSet _tokenSet_26 = new BitSet(_tokenSet_26_data_);
	private static final long _tokenSet_27_data_[] = { 2179072L, 0L };
	public static final BitSet _tokenSet_27 = new BitSet(_tokenSet_27_data_);
	private static final long _tokenSet_28_data_[] = { 2154498L, 0L };
	public static final BitSet _tokenSet_28 = new BitSet(_tokenSet_28_data_);
	private static final long _tokenSet_29_data_[] = { 3309568L, 0L };
	public static final BitSet _tokenSet_29 = new BitSet(_tokenSet_29_data_);
	private static final long _tokenSet_30_data_[] = { 2097154L, 0L };
	public static final BitSet _tokenSet_30 = new BitSet(_tokenSet_30_data_);
	private static final long _tokenSet_31_data_[] = { 2105344L, 0L };
	public static final BitSet _tokenSet_31 = new BitSet(_tokenSet_31_data_);
	private static final long _tokenSet_32_data_[] = { 262144L, 0L };
	public static final BitSet _tokenSet_32 = new BitSet(_tokenSet_32_data_);
	private static final long _tokenSet_33_data_[] = { 655362L, 0L };
	public static final BitSet _tokenSet_33 = new BitSet(_tokenSet_33_data_);
	private static final long _tokenSet_34_data_[] = { 393218L, 0L };
	public static final BitSet _tokenSet_34 = new BitSet(_tokenSet_34_data_);
	private static final long _tokenSet_35_data_[] = { 917506L, 0L };
	public static final BitSet _tokenSet_35 = new BitSet(_tokenSet_35_data_);
	private static final long _tokenSet_36_data_[] = { 131074L, 0L };
	public static final BitSet _tokenSet_36 = new BitSet(_tokenSet_36_data_);
	private static final long _tokenSet_37_data_[] = { 1048578L, 0L };
	public static final BitSet _tokenSet_37 = new BitSet(_tokenSet_37_data_);
	private static final long _tokenSet_38_data_[] = { 4198368L, 0L };
	public static final BitSet _tokenSet_38 = new BitSet(_tokenSet_38_data_);
	private static final long _tokenSet_39_data_[] = { 12586976L, 0L };
	public static final BitSet _tokenSet_39 = new BitSet(_tokenSet_39_data_);
	private static final long _tokenSet_40_data_[] = { 29364192L, 0L };
	public static final BitSet _tokenSet_40 = new BitSet(_tokenSet_40_data_);
	
	}
