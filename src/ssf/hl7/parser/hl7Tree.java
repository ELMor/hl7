// $ANTLR 2.7.1: "hl7.g" -> "hl7Tree.java"$

	package ssf.hl7.parser;

import antlr.TreeParser;
import antlr.Token;
import antlr.collections.AST;
import antlr.RecognitionException;
import antlr.ANTLRException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.collections.impl.BitSet;
import antlr.ASTPair;
import antlr.collections.impl.ASTArray;


public class hl7Tree extends antlr.TreeParser
       implements hl7ParserTokenTypes
 {
public hl7Tree() {
	tokenNames = _tokenNames;
}

	public final void mshADTA28(AST _t) throws RecognitionException {
		
		AST mshADTA28_AST_in = (AST)_t;
		
		try {      // for error handling
			AST __t122 = _t;
			AST tmp51_AST_in = (AST)_t;
			match(_t,MSH);
			_t = _t.getFirstChild();
			AST tmp52_AST_in = (AST)_t;
			match(_t,ADTA28);
			_t = _t.getNextSibling();
			{
			_loop124:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==BEGINFIELD)) {
					field(_t);
					_t = _retTree;
				}
				else {
					break _loop124;
				}
				
			} while (true);
			}
			AST tmp53_AST_in = (AST)_t;
			match(_t,ENDSEG);
			_t = _t.getNextSibling();
			_t = __t122;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
	}
	
	public final void field(AST _t) throws RecognitionException {
		
		AST field_AST_in = (AST)_t;
		
		try {      // for error handling
			AST __t126 = _t;
			AST tmp54_AST_in = (AST)_t;
			match(_t,BEGINFIELD);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CONTENT:
			{
				field_content(_t);
				_t = _retTree;
				{
				_loop129:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==BEGINREP)) {
						AST tmp55_AST_in = (AST)_t;
						match(_t,BEGINREP);
						_t = _t.getNextSibling();
						field_content(_t);
						_t = _retTree;
					}
					else {
						break _loop129;
					}
					
				} while (true);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t126;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
	}
	
	public final void field_content(AST _t) throws RecognitionException {
		
		AST field_content_AST_in = (AST)_t;
		
		try {      // for error handling
			component(_t);
			_t = _retTree;
			{
			_loop132:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==BEGINCOMP)) {
					AST tmp56_AST_in = (AST)_t;
					match(_t,BEGINCOMP);
					_t = _t.getNextSibling();
					component(_t);
					_t = _retTree;
				}
				else {
					break _loop132;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
	}
	
	public final void component(AST _t) throws RecognitionException {
		
		AST component_AST_in = (AST)_t;
		
		try {      // for error handling
			AST tmp57_AST_in = (AST)_t;
			match(_t,CONTENT);
			_t = _t.getNextSibling();
			{
			_loop135:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==BEGINSUBCOMP)) {
					AST tmp58_AST_in = (AST)_t;
					match(_t,BEGINSUBCOMP);
					_t = _t.getNextSibling();
					AST tmp59_AST_in = (AST)_t;
					match(_t,CONTENT);
					_t = _t.getNextSibling();
				}
				else {
					break _loop135;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"BEGINFIELD",
		"BEGINREP",
		"BEGINCOMP",
		"CONTENT",
		"BEGINSUBCOMP",
		"MSH",
		"ADTA28",
		"ENDSEG",
		"ADTA31",
		"ADTA34",
		"ORRO02",
		"ORUR01",
		"DFTP03",
		"EVN",
		"PID",
		"PV1",
		"IN1",
		"MRG",
		"ORC",
		"OBR",
		"OBX",
		"FT1",
		"ANY"
	};
	
	}
	
