package ssf.hl7.tcp;

public class socketClosedException
    extends Exception {
  socketClosedException(String s) {
    super(s);
  }

  socketClosedException() {
    super();
  }
}