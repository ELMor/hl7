package ssf.hl7.tcp;

public interface tcpable {
  public static final int MSAAcepted = 1;
  public static final int MSAError = 2;
  public static final int MSARejected = 3;
  public static final String MSAtxt[] = {
      "AA", "AE", "AR"};
}