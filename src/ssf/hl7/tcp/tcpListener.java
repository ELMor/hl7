
package ssf.hl7.tcp;

import java.io.*;
import java.sql.*;
import java.net.*;

import ssf.hl7.*;
import ssf.hl7.msg.*;

public class tcpListener implements Runnable {
	private int    				debug,po,so,sd;
	private Integer				sm;
	private String				jc,us,pw,sBeginBlock,sEndBlock;

	private ServerSocket 	ss=null;
	private Debug					dbg=null;

	public tcpListener(
				int debug,String jc, String us, String pw,
				int so, int sd,int po,Integer sm,String bb, String eb )
		throws Exception {
		this.dbg=new Debug(debug==1?true:false,null,0);
		this.debug=debug;
		this.jc=jc;
		this.us=us;
		this.pw=pw;
		this.so=so;
		this.sd=sd;
		this.po=po;
		this.sm=sm;
		sBeginBlock=bb;
		sEndBlock=eb;
		runningProc rp=new runningProc(0,0,jc,us,pw,so,sd);
		dbg.setLogo( rp.getLogo() );
		rp=null;
		dbg.deb("tcp.tcpListener escuchando puerto "+po);
		ss=new ServerSocket( po );
	}

	public void run(){
		int numh=1;
		try {
			Socket sckclient=ss.accept();
			dbg.ifdeb("Aceptando cliente "+sckclient.getInetAddress().toString());
			Server srv=new Server(numh,
														debug,jc,us,pw,so,sd,po,sm,
														sBeginBlock,
														sEndBlock,
													  new tcpBlockBroker(
																									so,sd,
																									sckclient.getOutputStream(),
																									sckclient.getInputStream(),
																									sBeginBlock,
																									sEndBlock
																								)
														);
			dbg.ifdeb("Arrancando hebra para soporte a nuevo socket");
			Thread th=new Thread(srv);
			th.start();
		}catch(Exception e){
			dbg.ifdeb(e.getMessage());
			e.printStackTrace();
		}
	}


}

 