package ssf.hl7;

import java.text.DecimalFormat;
import java.util.Calendar;

public class Debug {
  private boolean deb;
  private String logo;
  private int nmsg = 0;
  private int nh;
  private DecimalFormat nf1, nf2;

  public Debug(boolean debug, String logo, int nh) {
    deb = debug;
    nf1 = new DecimalFormat();
    nf1.applyPattern("000000");
    nf2 = new DecimalFormat();
    nf2.applyPattern("00");
    this.logo = logo;
    this.nh = nh;
  }

  public void ifdeb(String txt) {
    if (deb) {
      deb(txt);
    }
  }

  public void deb(String txt) {
    Calendar now = Calendar.getInstance();
    //now.setTimeInMillis(System.currentTimeMillis());
    System.out.println(
        logo
        + "["
        + "("
        + nf2.format(nh)
        + ")"
        + nf2.format(now.get(Calendar.YEAR) - 2000)
        + nf2.format(now.get(Calendar.MONTH))
        + nf2.format(now.get(Calendar.DAY_OF_MONTH))
        + nf2.format(now.get(Calendar.HOUR))
        + nf2.format(now.get(Calendar.MINUTE))
        + nf1.format(nmsg++)
        + "]"
        + txt
        );
  }

  public void setLogo(String txt) {
    logo = txt;
  }

  public String getLogo() {
    return logo;
  }

  public boolean getDeb() {
    return deb;
  }

  public String AsciiString(String k) {
    int i;

    StringBuffer sb = new StringBuffer();
    if (k == null) {
      return "[null]";
    }
    sb.append("[");
    for (i = 0; i < k.length(); i++) {
      if (i > 0) {
        sb.append("," + (int) (k.charAt(i)));
      }
      else {
        sb.append( (int) (k.charAt(i)));
      }
    }
    sb.append("]");
    return sb.toString();
  }

}