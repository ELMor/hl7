package ssf.hl7.msg;

import java.util.StringTokenizer;
import java.util.Vector;

public class Field {

  private Vector components;
  private Msg hl7Msg;

  public Field(Msg m, String s) {
    if (m == null) {
      m = new Msg("|", "^");
    }
    hl7Msg = m;
    String componentSeparator = hl7Msg.getComponentSeparator();
    components = new Vector();
    StringTokenizer stComp = new StringTokenizer(s, componentSeparator, true);
    if (stComp == null) {
      return;
    }
    int i = 1;
    String sComp;
    for (String sPrevComp = new String(componentSeparator);
         stComp.hasMoreTokens(); sPrevComp = sComp) {
      sComp = stComp.nextToken();
      if (!sComp.equals(componentSeparator)) {
        putComponent(i, new Component(sComp));
        i++;
      }
      else
      if (sPrevComp.equals(componentSeparator)) {
        putComponent(i, new Component(""));
        i++;
      }
    }

  }

  public void putComponent(int i, Component c) {
    if (i < 1) {
      return;
    }
    if (i <= components.size()) {
      components.setElementAt(c, i - 1);
    }
    else {
      for (int j = components.size(); j < i - 1; j++) {
        components.addElement(new Component(""));

      }
      components.addElement(c);
    }
  }

  public Component getComponent(int i) {
    if (i < 1) {
      return new Component("");
    }
    if (i > components.size()) {
      return new Component("");
    }
    else {
      return (Component) components.elementAt(i - 1);
    }
  }

  public String toString() {
    String s = new String("");
    for (int i = 0; i < components.size(); i++) {
      if (i == 0) {
        s = s + ( (Component) components.elementAt(i)).toString();
      }
      else {
        s = s + hl7Msg.getComponentSeparator() +
            ( (Component) components.elementAt(i)).toString();

      }
    }
    return s;
  }
}
