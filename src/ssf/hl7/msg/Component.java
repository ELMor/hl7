package ssf.hl7.msg;

public class Component {

  private String value;

  public Component(String s) {
    value = s;
  }

  public String toString() {
    return value;
  }
}
