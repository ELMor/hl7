package ssf.hl7;

class errorConfiguracionHL7
    extends Exception {
  public errorConfiguracionHL7() {
    super();
  }

  public errorConfiguracionHL7(String txt) {
    super(txt);
  }
}