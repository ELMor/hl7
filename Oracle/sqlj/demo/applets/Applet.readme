README FOR THE GENERIC SQLJ APPLET DEMO
=======================================

This demo shows how to translate and deploy a generic SQLJ applet.
This applet does not require the Oracle SQLJ runtime and can be
deployed through any browser that supports JDK 1.1 or later.

The applet demo consists of the following files:

  AppletMain.sqlj     - the SQLJ source for the applet
  AppletUI.java       - the user interface code for the applet
  AppletFrame.java    - a utility class that permits an applet to be
                        invoked as if it were an application
  connect.properties  - the connection properties for the applet - they
                        will be used if no connection parameters are
                        specified in the HTML file
  index.html          - HTML page that points to this generic applet demo,
                        and to the Oracle applet demo.
  Applet.html         - HTML page for invoking the applet in a browser

  jhttp.java          - a "micro" HTTP server written in Java for
                        testing the applet in a browser.

Note: The instructions in this README are specific to the Unix platform.
On a different environment, such as NT, you will have to use platform-
specific filenames, as well as platform-specific commands (for example,
replace "cp" with "copy").

NOTE: Throughout this example you can replace the use of
               runtime.zip
         with  runtime11.zip
      provided you employ the Oracle 8.1.7 JDBC driver.  In this case you
      can also drop the use of the -profile=false flag without affecting the
      runnability of your applet. The applet will be using the Oracle SQLJ
      runtime and not the generic SQLJ runtime.

(1) Translating and compiling the applet

% sqlj -profile=false AppletUI.java AppletFrame.java AppletMain.sqlj

  Note: The -profile=false flag disables the Oracle-specific customization.
  Your SQLJ code cannot use Oracle-specific types or features.
  (To be alerted about Oracle-specific features, you can enable portability
   flagging with the -warn=portable flag.)

  Note: As with most demos, in order to run SQLJ translation you should have
        .  (the current directory)
        $ORACLE_HOME/sqlj/lib/translator.zip
        $ORACLE_HOME/sqlj/lib/runtime.zip (see note above on use of this zip)
  and   $ORACLE_HOME/jdbc/lib/classes111.zip
  on your CLASSPATH.

  Note: If you are using JDK1.1.6 or later you will see the following
  warning message.

     AppletFrame.java:37: Note: Method boolean isActive() in class AppletFrame does not override the corresponding method in class java.awt.Window, which is private to a different package.
       public boolean isActive() { return true; };
                     ^
     1 warning

  You can either safely ignore this message, or your can comment out line 37
  in AppletFrame.java (however, in this case you will not be able to compile
  the demo under JDK1.1.5 or earlier).

(2) Testing the applet as an application

  a) Edit the file "connect.properties" to reflect your connection settings.

  b) % java AppletMain

         Note: If you see the following error message you forgot to set
         up the sqlj.url property in the connect.properties file.
         A SQL exception occurred: Io exception: Invalid number format for port number

  c) Close the application window to terminate the applet.


(3A) Running the applet on the WWW - Creating a code base

  a) Create a new directory "dist". This will become the new codebase
     directory for your applet.
     % mkdir dist

  b) Un-zip or un-jar the Oracle Thin JDBC driver under the "dist"
     directory.  For example:
     % cd ./dist
     % jar xf ${ORACLE_HOME}/jdbc/lib/classes111.zip

     Note: The zip file shown here contains the full Oracle JDBC driver
     (including the thin driver). You may want to reduce the size of your
     applet by only including those files required for the thin driver.
     Please consult your JDBC documentation for the name and location
     of the distribution file that contains the JDBC thin driver only.
     You can also download a version of the Oracle thin JDBC driver from:
        http://www.oracle.com/java/jdbc/

  c) Unzip or unjar the SQLJ runtime under the "dist" directory.
     For example:
     % jar xf ${ORACLE_HOME}/sqlj/lib/runtime.zip
     % cd ..

  d) Re-translate your applet so that all .class and .ser files are
     placed under the dist hierarchy.

     % sqlj -profile=false -ser2class -d=dist AppletUI.java AppletFrame.java AppletMain.sqlj

     Note: Some browsers cannot handle *.ser resources. For maximum
     browser compatibility of your applet, you should use the -ser2class
     option to convert all *.ser resources into class files.

     Note: You can also copy the *.class files by hand.
     However, using the -d option will ensure that all .class and
     .ser files will be automatically placed in the proper location.
     This is especially important when your applet uses explicit
     package names.

  e) Copy the "connection.properties" resource to the directory 
     where your main applet class AppletMain.class resides.
     For example:

     % cp connect.properties ./dist

     Note: Ensure that you use a JDBC thin URL 
     (jdbc:oracle:thin:@<host>:<port>:<sid>) in your
     connect.properties file.

     Note: If you specify appropriate PARAMeters in the APPLET
     tag of your .html file, the appletviewer and browsers will use
     them, rather than the settings in the connect.properties file.

  f) (optional) Test your code base as follows.

     - only include the root of the codebase in your CLASSPATH.
       % setenv CLASSPATH ./dist 

     - re-run the applet from the commandline
       % java AppletMain

(3B) Running the applet on the WWW - Creating a distribution jar

   a) Jar up all files in the codebase

      % cd ./dist
      % jar c0f Applet.jar *
      % cd ..

   b) (optional) Test your distribution jar as follows.

     - only include the applet jar in your CLASSPATH.
       % setenv CLASSPATH ./dist/Applet.jar 

     - re-run the applet from the commandline
       % java AppletMain


(3C) Running the applet on the WWW - Setting up an HTML server

  (Optional) Do the following in a separate shell window.

  a) Compile the "micro" web server.

     % javac jhttp.java

  b) Start serving HTTP pages with the "micro" web server. 
     The syntax is:  java jhttp <root-directory> <port>

     % java jhttp . 8080

     Note: Ensure that "." is in your CLASSPATH when you run this.

     Note: The jhttp program is not meant to be a production
     web server.  It might help you during preliminary testing of
     applets.

  c) You must type <CTRL>-C to terminate the HTTP server.


(3D) Running the applet on the WWW - Using appletviewer

  a) Edit file Applet.html to reflect your connection settings.
     If you remove the connection PARAMeters from the APPLET tag, the
     applet will use the settings from the connect.properties resource.

     Note: you must run your HTML server and the database to which
     you connect on the same machine.

     Note: The appletviewer in /usr/bin on Solaris does not appear to
     be able to pick up the connect.properties file in the jar. In this
     case you may want to use the PARAMeter setting in the APPLET tag.

  b) Remove all entries in your CLASSPATH to ensure that you only pick
     up classes from the jar file.

     % setenv CLASSPATH

  c) Test the applet via appletviewer

      % appletviewer http://<yourhost>:8080/Applet.html

         Note: Ensure that the hostname in this URL and the hostname
         that you specified in the APPLET tag are the same. Otherwise
         you may get a security exception.

(3E) Running the applet on the WWW - Using a browser

  a) Make sure you have edited the file Applet.html as per the instructions
     in step 3D-a.

  b) You must use a JDK 1.1 - compliant browser, such as Netscape 4.0 and
     later, or Internet Explorer 4.0 and later.
     You can also use earlier browsers if you use Sun's Java plug-in.

     Type the applet URL into your browser and test the applet. For example:

       http://<yourhost>:8080/Applet.html

     Note: If you are using Netscape 4.0X and you get a ClassNotFoundException
     for JDK1.1 AWT classes, you need to download Netscape's JDK 1.1 patch.
    
     You can get this patch from:

       http://help.netscape.com/filelib.html 

     Note: If you want the applet to connect to a different database
     server, you have to sign the applet (see your JDK documentation,
     or at http://java.sun.com/security/signExample/).  Alternatively,
     you can use a network redirector (such as Oracle's Net8 product).
     Alternatively, you may be able to employ browser-specific APIs
     (for example, the UniversalConnect privilege in Netscape's 
      PrivilegeManager) that you can call explicitly to give your applet
     the capability to connect to other machines on the network.
