/*
 * This applet is the UI to prompt for a user name from the emp table.
 * The result is displayed in a test area in the browser.
 */

// Import the java classes used in applets
import java.awt.Button;
import java.awt.Component;
import java.awt.Label;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.applet.Applet;

public class AppletUI extends Applet
{

  // The button to push for executing the query
  protected Button Query_button, Delete_button;

  // The place where to dump the query result
  protected TextArea output;

  // The label for the Input
  protected Label query_name_label, delete_name_label;

  // More Input TextFields
  protected TextField query_name_field, delete_name_field;

  // Create the User Interface
  public void init()
  {
    GridBagLayout gridbag = new GridBagLayout();

    GridBagConstraints constraints = new GridBagConstraints();
    setFont(new Font("TimesRoman", Font.BOLD, 18));
    constraints.anchor = GridBagConstraints.NORTHEAST;
    setLayout(gridbag);
    constraints.gridheight = 10;
    constraints.gridwidth = 5;

    query_name_label = new Label("Enter Employee Name to Search: ");
    addFormComponent(gridbag, query_name_label, constraints);

    query_name_field = new TextField(15);
    addFormComponent(gridbag, query_name_field, constraints);
    constraints.gridwidth = GridBagConstraints.REMAINDER;

    Query_button = new Button("Query");
    addFormComponent(gridbag, Query_button, constraints);

    output = new TextArea(10,55);
    addFormComponent(gridbag, output, constraints);

    constraints.gridwidth = 5;
    delete_name_label = new Label("Enter Employee Name to Delete: ");
    addFormComponent(gridbag, delete_name_label, constraints);

    delete_name_field = new TextField(15);
    addFormComponent(gridbag, delete_name_field, constraints);

    Delete_button = new Button("Delete");
    addFormComponent(gridbag, Delete_button, constraints);

  }

  private void addFormComponent(GridBagLayout grid, Component comp, GridBagConstraints c)
  {
    grid.setConstraints(comp, c);
    add(comp);
  }
  
}
