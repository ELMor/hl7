
create or replace package SQLJRefCursDemo as
  type EmpCursor is ref cursor;
  procedure RefCursProc( name VARCHAR,
                         no NUMBER,
                         empcur OUT EmpCursor);

  function RefCursFunc (name VARCHAR, no NUMBER)  return EmpCursor;

end SQLJRefCursDemo;
/

create or replace package body SQLJRefCursDemo is

  procedure RefCursProc( name VARCHAR,
	   		 no NUMBER,
			 empcur OUT EmpCursor)
     is begin
       insert into emp (ename, empno) values (name, no);
       open empcur for select ename, empno from emp
                       order by empno;
     end;

  function RefCursFunc (name VARCHAR, no NUMBER) return EmpCursor  is
    empcur EmpCursor;
    begin
       insert into emp (ename, empno) values (name, no);
       open empcur for select ename, empno from emp
                       order by empno;
       return empcur;
     end;
end SQLJRefCursDemo;
/
exit
/
