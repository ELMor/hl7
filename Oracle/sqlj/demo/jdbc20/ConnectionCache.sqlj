/**
 Using connection caching in SQLJ.

 Adapted from the JDBC ConnectionCache demos.
 **/

import java.sql.SQLException;
import java.sql.Connection;
import java.util.PropertyResourceBundle;

class ConnectionCache
{
  private static final int NUM_CONTEXTS = 5;
  private static sqlj.runtime.ref.DefaultContext[] ctx = null;
  private static final int MAX_LIMIT    = 3;

/*
 Demo that illustrates the DYNAMIC_SCHEME of OracleConnectionCacheImpl.

 This is the default scheme. New connections can be 
 created beyond the Max limit upon request but closed and freed when the 
 logical connections are closed. When all the connections are active and 
 busy, requests for new connections willend up creating new physical 
 connections. But these physical connections are closed when the
 corresponding logical connections are closed. A typical grow and shrink 
 scheme. 
 */
  public static void dynamicSchemeCaching(String url, String user, String pwd)
       throws SQLException
  {
    System.out.println("*** DYNAMIC_SCHEME demo");

    oracle.jdbc.pool.OracleConnectionCacheImpl ods = new oracle.jdbc.pool.OracleConnectionCacheImpl();
    ods.setURL(url);
    ods.setUser(user);
    ods.setPassword(pwd);

    // Set the Max Limit
    ods.setMaxLimit (MAX_LIMIT);

    System.out.println("Creating "+NUM_CONTEXTS+" contexts...");
    ctx = new sqlj.runtime.ref.DefaultContext[NUM_CONTEXTS];
    for (int i=0; i<NUM_CONTEXTS; i++) { 
      try
      {
        Connection conn = ods.getConnection();
        conn.setAutoCommit(false);
        ctx[i] = new sqlj.runtime.ref.DefaultContext(conn);
        System.out.println("DefaultContext "+i+" succeeded.");
      }
      catch (Exception e)
      {
        System.out.println("DefaultContext "+i+" failed: "+e.toString()+".");
      }
    }
    printState(ods);


    System.out.println("Closing three contexts...");
    for (int i=0; i<3; i++) {
       if (ctx[i]!=null) ctx[i].close();
    }
    printState(ods);

    System.out.println("Closing the DataSource...");
    ods.close();
    printState(ods);
  }

/*
 Demo that illustrates the FIXED_RETURN_NULL_SCHEME of OracleConnectionCacheImpl.

 Fixed with no wait:  At no instance are there  more active connections
 than the the Max limit.  Requests for new connections beyond the Max
 limit will return a null connection. 
 **/

  public static void fixedReturnNullCaching(String url, String user, String pwd)
       throws SQLException
  {
    System.out.println("*** FIXED_RETURN_NULL_SCHEME demo");

    // Create a oracle.jdbc.pool.OracleConnectionPoolDataSource as an factory
    // of PooledConnections for the cache.
    oracle.jdbc.pool.OracleConnectionPoolDataSource ocpds =
                         new oracle.jdbc.pool.OracleConnectionPoolDataSource();
    ocpds.setURL(url);
    ocpds.setUser(user);
    ocpds.setPassword(pwd);

    oracle.jdbc.pool.OracleConnectionCacheImpl ods =
          new oracle.jdbc.pool.OracleConnectionCacheImpl(ocpds);

     // Set the Max Limit
    ods.setMaxLimit (MAX_LIMIT);

    // Set the Scheme
    ods.setCacheScheme (oracle.jdbc.pool.OracleConnectionCacheImpl.FIXED_RETURN_NULL_SCHEME);

    System.out.println("Creating "+NUM_CONTEXTS+" contexts...");
    ctx = new sqlj.runtime.ref.DefaultContext[NUM_CONTEXTS];
    for (int i=0; i<NUM_CONTEXTS; i++) { 
      try
      {
        Connection conn = ods.getConnection();
        conn.setAutoCommit(false);
        ctx[i] = new sqlj.runtime.ref.DefaultContext(conn);
        System.out.println("DefaultContext "+i+" succeeded.");
      }
      catch (Exception e)
      {
        System.out.println("DefaultContext "+i+" failed: "+e.toString()+".");
      }
    }
    printState(ods);


    System.out.println("Closing two contexts...");
    for (int i=0; i<2; i++) {
       if (ctx[i]!=null) ctx[i].close();
    }
    printState(ods);

    System.out.println("Closing the DataSource...");
    ods.close();
    printState(ods);
  }

 /**
  Main method for ConnectionCache demo program.
  **/

 public static void main(String[] args) throws java.io.IOException
 {
   
   PropertyResourceBundle p = 
     new PropertyResourceBundle
            (ConnectionCache.class.getClassLoader()
               .getSystemResourceAsStream("connect.properties"));

   String url  = p.getString("sqlj.url");
   String user = p.getString("sqlj.user");
   String pwd  = p.getString("sqlj.password");
   
   try {
     dynamicSchemeCaching(url, user, pwd);
   } catch (SQLException e) {
     System.out.println("Error: "+e);
   } 

   try {
     fixedReturnNullCaching(url, user, pwd);
   } catch (SQLException e) {
     System.out.println("Error: "+e);
   } 

 }

 /*
  Print out the current state of the cache
  */
  private static void printState(oracle.jdbc.pool.OracleConnectionCacheImpl ods)
  {
    System.out.println("Active size: " + ods.getActiveSize());
    System.out.println("Cache Size : " + ods.getCacheSize());
    System.out.print  ("Contexts   : ");
    for (int i=0; i < NUM_CONTEXTS; ++i )
    {
      System.out.print(" ctx["+i+"]="+
                      (  (ctx[i]==null)      ? "null  "
                      : ((ctx[i].isClosed()) ? "closed"
                      :                        "open  ")));
    }
    System.out.println();
  }

}
