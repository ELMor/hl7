import java.sql.*;
import oracle.sqlj.runtime.Oracle;

public class JavaSerialization
{
 /***
  Demonstration of serializing Java objects into database columns.

  In Oracle 8.1.7 and earlier there is no support for SQL types
  that implement "SQLJ Part 2", for storing Java objects directly
  in SQL columns (JAVA_OBJECT types).
  
  An Oracle SQLJ extension, however, provides for storing and
  retrieving (serializable) Java object instances directly in RAW
  and BLOB column.  The Java types that are mapped to RAW (or
  -respectively- BLOB) are specified in a connection context
  specific type map in oracle-class (rather class) entries.
  ***/

  #sql public static context SerContext with (typeMap="JavaSerializationMap");


  public static class AnObject implements java.io.Serializable
  {
    public int i;
    public String s;
    public String toString() { return "AnObject{"+i+","+s+"}"; } 
  }

  public static class AnotherObject extends java.math.BigDecimal implements java.io.Serializable
  {
    public AnotherObject(int i) { super(i*i); }
    public String toString() { return "AnotherObject{"+super.toString()+"}"; }
  }


  #sql public static iterator SerIter with (typeMap="JavaSerializationMap") (int n, AnObject r, AnotherObject b);

/***
  Main demo entry point
 ***/
 public static void main(String[] args) throws SQLException
 {
   try
   {
     Oracle.connect(JavaSerialization.class,"connect.properties");
  
     SerContext ctx = new SerContext(sqlj.runtime.ref.DefaultContext.getDefaultContext().getConnection());
  
     String[] words = new String[] { "zero", "un", "zwei", "tres" };
  
     AnObject o = new AnObject();
     AnotherObject ao;
  
     System.out.println("Inserting serialized Java objects");
     for (int i=0; i<words.length; i++)
     {
       o.i = i; o.s = words[i];
       ao = new AnotherObject(i);
       
       #sql [ctx] { INSERT INTO SER_TAB VALUES(:i, :o, :ao) };
     }
  
     System.out.println("Retrieving serialized Java objects");
     SerIter si;
     #sql [ctx] si = { SELECT * FROM SER_TAB };
  
     while (si.next()) {
       System.out.println("#"+si.n()+"  "+si.r()+"  "+si.b());
     }
     si.close();

   }
   finally
   {
     try { #sql { ROLLBACK }; } catch (SQLException exn) { };
     Oracle.close();
   }
 }

}
