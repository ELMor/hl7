import java.sql.SQLException;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.jpub.runtime.MutableStruct;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ConnectionContext;
import java.sql.Connection;

public class JavaStructPersonT implements ORAData, ORADataFactory
{
  public static final String _SQL_NAME = "SCOTT.JAVA_STRUCT_PERSON_T";
  public static final int _SQL_TYPECODE = OracleTypes.JAVA_STRUCT;

  /* connection management */
  protected DefaultContext __tx = null;
  protected Connection __onn = null;
  public void setConnectionContext(DefaultContext ctx) throws SQLException
  { release(); __tx = ctx; }
  public DefaultContext getConnectionContext() throws SQLException
  { if (__tx==null)
    { __tx = (__onn==null) ? DefaultContext.getDefaultContext() : new DefaultContext(__onn); }
    return __tx;
  };
  public Connection getConnection() throws SQLException
  { return (__onn==null) ? ((__tx==null) ? null : __tx.getConnection()) : __onn; }
  public void release() throws SQLException
  { if (__tx!=null && __onn!=null) __tx.close(ConnectionContext.KEEP_CONNECTION);
    __onn = null; __tx = null;
  }

  protected MutableStruct _struct;

  private static int[] _sqlType =  { 2,12,91 };
  private static ORADataFactory[] _factory = new ORADataFactory[3];
  protected static final JavaStructPersonT _JavaStructPersonTFactory = new JavaStructPersonT(false);

  public static ORADataFactory getORADataFactory()
  { return _JavaStructPersonTFactory; }
  /* constructors */
  protected JavaStructPersonT(boolean init)
  { if (init) _struct = new MutableStruct(new Object[3], _sqlType, _factory); }

  public JavaStructPersonT()
  { this(true); __tx = DefaultContext.getDefaultContext(); }
  public JavaStructPersonT(DefaultContext c) 
  { this(true); __tx = c; }
  public JavaStructPersonT(Connection c)
  { this(true); __onn = c; }

  public JavaStructPersonT(java.math.BigDecimal ssn,
                           String firstname, String lastname,
                           java.sql.Timestamp dot)
   throws SQLException
   {
    this(true);
    setSsn(ssn);
    setFirstName(firstname);
    setLastName(lastname);
    setDot(dot);
  }

  /* ORAData interface */
  public Datum toDatum(Connection c) throws SQLException
  {
    if (__tx!=null && __onn!=c) release();
    __onn = c;
    beforeJavaToSqlConversion();
    return _struct.toDatum(c, _SQL_NAME);
  }


  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  { return create(null, d, sqlType); }
  public void setFrom(JavaStructPersonT o) throws SQLException
  { setContextFrom(o); setValueFrom(o); }
  protected void setContextFrom(JavaStructPersonT o) throws SQLException
  { release(); __tx = o.__tx; __onn = o.__onn; }
  protected void setValueFrom(JavaStructPersonT o) { _struct = o._struct; }
  protected ORAData create(JavaStructPersonT o, Datum d, int sqlType) throws SQLException
  {
    if (d == null) { if (o!=null) { o.release(); }; return null; }
    if (o == null) o = new JavaStructPersonT(false);
    o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
    o.__onn = ((STRUCT) d).getJavaSqlConnection();
    o.afterSqlToJavaConversion();
    return o;
  }

  /* Mapping the SQL NAME attribute to two Java properties */

  private static boolean inDatabase =
          System.getProperty("oracle.server.version") != null;

  private void afterSqlToJavaConversion() throws SQLException
  { String name = (String) _struct.getAttribute(1);
    int pos = name.indexOf(" ");
    if (pos<0)
    { m_firstname = name; m_lastname = ""; }
    else
    { m_firstname = name.substring(0,pos);
      m_lastname = name.substring(pos+1);
    }
    if (inDatabase)
    {
      m_firstname = m_firstname + "DB ";
      m_lastname = m_lastname + "DB";
    }
  }

  // Materialize names differently in the database
  private void beforeJavaToSqlConversion() throws SQLException
  { 
    { _struct.setAttribute(1, m_firstname +
                              ((m_lastname.equals("")) ? m_lastname
                                                    : " " + m_lastname)); }
  }
  private String m_firstname;
  private String m_lastname;


  public String getFirstName()
  { return m_firstname; }

  public String getLastName()
  { return m_lastname; }

  public void setFirstName(String name)
  { m_firstname = name; }

  public void setLastName(String name)
  { m_lastname = name; }

  /* standard accessor methods */

  public java.math.BigDecimal getSsn() throws SQLException
  { return (java.math.BigDecimal) _struct.getAttribute(0); }

  public void setSsn(java.math.BigDecimal ssn) throws SQLException
  { _struct.setAttribute(0, ssn); }

  public java.sql.Timestamp getDot() throws SQLException
  { return (java.sql.Timestamp) _struct.getAttribute(2); }

  public void setDot(java.sql.Timestamp dot) throws SQLException
  { _struct.setAttribute(2, dot); }


  /* server-side methods */

  public String getDbFirstName ()
  throws SQLException
  {
    JavaStructPersonT __jPt_temp = this;
    String __jPt_result;
    #sql [getConnectionContext()] {
      BEGIN
      :OUT __jPt_result := :__jPt_temp.FIRSTNAME();
      END;
    };
    return __jPt_result;
  }

  public String getDbLastName ()
  throws SQLException
  {
    JavaStructPersonT __jPt_temp = this;
    String __jPt_result;
    #sql [getConnectionContext()] {
      BEGIN
      :OUT __jPt_result := :__jPt_temp.LASTNAME();
      END;
    };
    return __jPt_result;
  }

}
