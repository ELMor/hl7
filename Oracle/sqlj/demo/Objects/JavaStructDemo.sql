-- Script to create the SQLJ object type JAVA_STRUCT_PERSON_T
--  based on the Java type JavaStructPersonT

set echo on;

DROP table JAVA_STRUCT_PERSON_TAB;
DROP type  JAVA_STRUCT_PERSON_T force;

CREATE type JAVA_STRUCT_PERSON_T AS OBJECT
  EXTERNAL NAME 'JavaStructPersonT'
  LANGUAGE JAVA USING OraData
  (ssn  NUMBER,
   name VARCHAR2(40),
   dot  DATE,

   MEMBER FUNCTION firstname return VARCHAR2
        external name 'getFirstName() return java.lang.String',
   MEMBER FUNCTION lastname  return VARCHAR2
        external name 'getLastName() return java.lang.String'
  );
/

CREATE TABLE JAVA_STRUCT_PERSON_TAB of JAVA_STRUCT_PERSON_T;

COMMIT;

exit;
