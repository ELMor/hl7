import java.util.Enumeration;
import java.util.Vector;
import java.io.ByteArrayInputStream;
import sqlj.util.io.StringCharStream;
import sqlj.framework.error.ErrorLog;
import sqlj.framework.error.JSError;
import sqlj.framework.error.LogEntry;

/**
SQLUtil uses SQLParser to parse SQL SELECT statements and
<ul>
 <li> either insert (1=2) AND ... at the beginning of every WHERE clause, or
 <li> remove the entire contents of the WHERE clause with (1=2).
</ul>
Whenever no top-level WHERE clause exists, WHERE (1=2) will be inserted.

@author jbasu, erohwedd
**/

 
public class SQLUtil extends SQL {

  // Manipulate SQL query strings in order to obtain a result set for
  // semantic analysis

  public SQLUtil(String s) {
        super(new SQLTokenManager(new StringCharStream(s)));
  }


  public static void main(String[] args)
  {
    StringBuffer sb = new StringBuffer();

    for (int i=0; i<args.length; i++)
    {
      sb.append(" ");
      sb.append(args[i]);
    }

    SQLUtil su = new SQLUtil(sb.toString());

    try
    {
      su.sql_statement();
      System.out.println("SQL statement accepted.");
    }
    catch (ParseException exn)
    {
      exn.printStackTrace();
      System.out.println("Error in SQL statement: "+exn);
    }
  }
}
