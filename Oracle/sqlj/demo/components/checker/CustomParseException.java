public class CustomParseException extends ParseException {

  public CustomParseException(ParseException pe)
  {
    super("");
    specialConstructor = pe.specialConstructor;
    currentToken = pe.currentToken;
    expectedTokenSequences = pe.expectedTokenSequences;
    tokenImage = pe.tokenImage;
    eol = pe.eol;
  }

  public String getMessage(String stmt)
  {
    if (!specialConstructor)
    {
      return super.getMessage();
    }

    String retval = "statement fails SQL parsing. ";

    String expected = "";
    int maxSize = 0;
    for (int i = 0; i < expectedTokenSequences.length; i++)
    {
      if (maxSize < expectedTokenSequences[i].length)
      {
        maxSize = expectedTokenSequences[i].length;
      }
      for (int j = 0; j < expectedTokenSequences[i].length; j++)
      {
        expected += tokenImage[expectedTokenSequences[i][j]] + " ";
      }
      if (expectedTokenSequences[i][expectedTokenSequences[i].length - 1] != 0)
      {
        expected += "...";
      }
      expected += eol + ((i < expectedTokenSequences.length - 1) ? "    " : "");
    }

    retval += "Encountered \"";
    Token tok = currentToken.next;
    for (int i = 0; i < maxSize; i++) {
      if (i != 0) retval += " ";
      if (tok.kind == 0) {
        retval += tokenImage[0];
        break;
      }
      retval += add_escapes(tok.image);
      tok = tok.next; 
    }
    retval += "\"." + eol;

    if (expectedTokenSequences.length == 1)
    {
      retval += "Was expecting:" + eol + "    ";
    }
    else
    {
      retval += "Was expecting one of:" + eol + "    ";
    }

    int start = currentToken.next.beginLine;
    int end   = currentToken.next.endLine;

    String front = stmt.substring(0,start);
    String middle= stmt.substring(start,end);
    String back  = stmt.substring(end);

    retval += expected + "SQL statement: "+ front + " >>>> " + middle + " <<<< " + back +"\n";
    return retval;
  }

}
