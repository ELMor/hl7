-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
----- File:    WrDbmsUtil.sql
-----
----- Purpose: publish the SYS.DBMS_UTIL package to Java
-----
----- Contains:
-----
-----    JPublisher type definitions for scalar index-by tables
-----      Note that this means JDBC-OCI is a requirement
-----
-----    SQL type definitions for PL/SQL types
-----
-----      PL/SQL: DBMS_UTIL.INSTRANCE_RECORD
-----      SQL:    wr_dbmsutil_instance_record
-----
-----      PL/SQL: DBMS_UTIL.INSTRANCE_TABLE
-----      SQL:    wr_dbmsutil_instance_table
-----
-----    Definition of PL/SQL package WR_DBMSUTIL
-----      with conversion functions between the PL/SQL
-----      and the SQL types above
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------

--
-- Directives for JPublisher how to generate the PL/SQL wrapper
-- 
-- jpub.plsqlfile=WrDbmsUtilPlsql.sql
-- jpub.plsqlpackage=WR_DBMSUTIL_PLSQL
--


--
-- Remove everything first
--

drop package WR_DBMSUTIL_PLSQL;
drop package wr_dbmsutil;

drop type wr_dbmsutil_instance_table;
drop type wr_dbmsutil_instance_record;

--
-- More JPublisher directives are interspersed below
--

-- type uncl_array IS TABLE OF VARCHAR2(227) INDEX BY BINARY_INTEGER;
-- Lists of "USER"."NAME"."COLUMN"@LINK should be stored here. 
--
--  jpub.addtypemap=SYS.DBMS_UTILITY.UNCL_ARRAY:String[1000](227)

-- type lname_array IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
--
--  jpub.addtypemap=SYS.DBMS_UTILITY.LNAME_ARRAY:String[1000](4000)

-- type name_array IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;
-- Lists of NAME should be stored here. 
--
--  jpub.addtypemap=SYS.DBMS_UTILITY.NAME_ARRAY:String[1000](30)

-- type dblink_array IS TABLE OF VARCHAR2(128) INDEX BY BINARY_INTEGER;
-- Lists of database links should be stored here. 
--
--  jpub.addtypemap=SYS.DBMS_UTILITY.DBLINK_ARRAY:String[1000](128)


-- TYPE index_table_type IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;
-- The order in which objects should be generated is returned here. 
--
--  jpub.addtypemap=SYS.DBMS_UTILITY.INDEX_TABLE_TYPE:int[1000]

-- TYPE number_array IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
-- The order in which objects should be generated is returned here for users. 
--
--  jpub.addtypemap=SYS.DBMS_UTILITY.NUMBER_ARRAY:int[1000]

--  TYPE instance_record IS RECORD (
--       inst_number   NUMBER,
--       inst_name     VARCHAR2(60));
--
--
-- Note: we automatically publish a Java wrapper for the SQL
--       type. Commend out the following line if this is not expected.
-- jpub.sql=WR_DBMSUTIL_INSTANCE_RECORD:WrDbmsUtilInstanceRecord
-- jpub.addtypemap=SYS.DBMS_UTILITY.INSTANCE_RECORD:WrDbmsutilInstanceRecord:\
--                 WR_DBMSUTIL_INSTANCE_RECORD:\
--                 WR_DBMSUTIL.SQL2INSTANCE_RECORD:\
--                 WR_DBMSUTIL.INSTANCE_RECORD2SQL

create TYPE wr_dbmsutil_instance_record as object (
         wr_inst_number   NUMBER,
         wr_inst_name     VARCHAR2(60));
/

--  TYPE instance_table IS TABLE OF instance_record INDEX BY BINARY_INTEGER;
--
-- Note: we automatically publish a Java wrapper for the SQL
--       type. Commend out the following line if this is not expected.
-- jpub.sql=WR_DBMSUTIL_INSTANCE_TABLE:WrDbmsUtilInstanceTable
-- jpub.addtypemap=SYS.DBMS_UTILITY.INSTANCE_TABLE:WrDbmsUtilInstanceTable:\
--                 WR_DBMSUTIL_INSTANCE_TABLE:\
--                 WR_DBMSUTIL.SQL2INSTANCE_TABLE:\
--                 WR_DBMSUTIL.INSTANCE_TABLE2SQL

create TYPE wr_dbmsutil_instance_table as table of wr_dbmsutil_instance_record;
/


create or replace package wr_dbmsutil as
   function instance_record2sql (r SYS.DBMS_UTILITY.INSTANCE_RECORD)
            return wr_dbmsutil_instance_record;
   function sql2instance_record (r wr_dbmsutil_instance_record)
            return SYS.DBMS_UTILITY.INSTANCE_RECORD;
   function instance_table2sql (r SYS.DBMS_UTILITY.INSTANCE_TABLE)
            return wr_dbmsutil_instance_table;
   function sql2instance_table (r wr_dbmsutil_instance_table)
            return SYS.DBMS_UTILITY.INSTANCE_TABLE;
end wr_dbmsutil;
/

create or replace package body wr_dbmsutil is
   function instance_record2sql (r SYS.DBMS_UTILITY.INSTANCE_RECORD)
            return wr_dbmsutil_instance_record is
   begin
      return wr_dbmsutil_instance_record(r.inst_number, r.inst_name);
   end instance_record2sql;

   function sql2instance_record (r wr_dbmsutil_instance_record)
            return SYS.DBMS_UTILITY.INSTANCE_RECORD is
    res SYS.DBMS_UTILITY.INSTANCE_RECORD;
   begin
    if r IS NOT NULL
    then
      res.inst_number := r.wr_inst_number;
      res.inst_name   := r.wr_inst_name;
    end if;
    return res;
   end sql2instance_record;

   function instance_table2sql (r SYS.DBMS_UTILITY.INSTANCE_TABLE)
            return wr_dbmsutil_instance_table is
     tab wr_dbmsutil_instance_table := wr_dbmsutil_instance_table();
   begin
     FOR i IN 1..r.LAST LOOP
        tab(i) := instance_record2sql(r(i));
     END LOOP;
     return tab;
   end instance_table2sql;

   function sql2instance_table (r wr_dbmsutil_instance_table)
            return SYS.DBMS_UTILITY.INSTANCE_TABLE is
     res SYS.DBMS_UTILITY.INSTANCE_TABLE;
   begin
     FOR i IN 1..r.LAST LOOP
       res(i) := sql2instance_record(r(i));
     END LOOP;
     return res;
   end sql2instance_table;
end wr_dbmsutil;
/

commit;
exit
