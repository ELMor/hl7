/* Copyright (c) Oracle Corporation 1999. All Rights Reserved. */

import sqlj.runtime.ref.DefaultContext;
import oracle.sqlj.runtime.Oracle;


public class TestMyRationalC
{

  public static void main(String[] args) throws java.sql.SQLException
  {
    int n = 5;
    int d = 10;
    MyRationalC r = new MyRationalC(5, 10);

    int g = r.gcd(n, d);
    System.out.println("gcd: " + g);

    float f = r.toReal();
    System.out.println("real value: " + f);

    MyRationalC s = r.plus(r); 
    System.out.println("sum: " + s);

    s.normalize();
    System.out.println("sum: " + s);
  } 
}
