
/* Copyright (c) Oracle Corporation 1999. All Rights Reserved. */

import java.sql.SQLException;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.jpub.runtime.MutableStruct;
import sqlj.runtime.ConnectionContext;
import sqlj.runtime.ref.DefaultContext;
import java.sql.Connection;

public class MyRationalO extends JPubRationalO
                      implements ORAData, ORADataFactory
{
  /* _SQL_NAME inherited from JPubRationalO */

  /* _SQL_TYPECODE inherited from JPubRationalO */

  static final MyRationalO _MyRationalOFactory = new MyRationalO();
  public static ORADataFactory getORADataFactory()
  {
    return _MyRationalOFactory;
  }

  /* constructors */
  public MyRationalO()
  {
    super();
  }

  public MyRationalO(DefaultContext c) throws SQLException  
  {
    super(c);
  }

  public MyRationalO(Connection c) throws SQLException  
  {
    super(c);
  }

  /* ORAData interface */
  /* toDatum() inherited from JPubRationalO */

  /* ORADataFactory interface */
  public ORAData create(Datum d, int sqlType) throws SQLException
  {
    return create(new MyRationalO(), d, sqlType);
  }

  /* accessor methods inherited from JPubRationalO */

  /* additional method not in base class */
  public String toString()
  {
    try
    {
       return getNumerator().toString() + "/" + getDenominator().toString();
    }
    catch (SQLException e)
    {
       return null;
    }
  }
}
