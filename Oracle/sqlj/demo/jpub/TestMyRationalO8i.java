
/* Copyright (c) Oracle Corporation 1999, 2000. All Rights Reserved. */

import sqlj.runtime.ref.DefaultContext;
import oracle.sqlj.runtime.Oracle;


public class TestMyRationalO8i
{

  public static void main(String[] args) throws java.sql.SQLException
  {
    DefaultContext dc;
    dc = Oracle.connect(new TestMyRationalO8i().getClass(),
                        "connect.properties");

    if (dc == null)
    {
       System.err.println("Can't connect!");
       return;
    }

    Integer n = new Integer(5);
    Integer d = new Integer(10);
    MyRationalO8i r = new MyRationalO8i(dc);
    r.setNumerator(n);
    r.setDenominator(d);

    Integer g = r.gcd(n, d);
    System.out.println("gcd: " + g);

    Float f = r.toreal();
    System.out.println("real value: " + f);

    MyRationalO8i s = r.plus(r); 
    System.out.println("sum: " + s);

    s = s.normalize();
    System.out.println("sum: " + s);
  } 
}
