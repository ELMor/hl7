
/* Copyright (c) Oracle Corporation 1999. All Rights Reserved. */

import java.sql.SQLException;
import oracle.sql.CustomDatum;
import oracle.sql.CustomDatumFactory;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.jpub.runtime.MutableStruct;
import sqlj.runtime.ConnectionContext;
import java.sql.Connection;

public class MyRationalO8i extends JPubRationalO8i
                      implements CustomDatum, CustomDatumFactory
{
  /* _SQL_NAME inherited from JPubRationalO8i */

  /* _SQL_TYPECODE inherited from JPubRationalO8i */

  /* _ctx inherited from JPubRationalO8i */

  /* _sqlType inherited from JPubRationalO8i */ 

  /* _struct inherited from JPubRationalO8i */

  static final MyRationalO8i _MyRationalO8iFactory = new MyRationalO8i();
  public static CustomDatumFactory getFactory()
  {
    return _MyRationalO8iFactory;
  }

  /* constructors */
  public MyRationalO8i()
  {
    super();
  }

  public MyRationalO8i(ConnectionContext c) throws SQLException  
  {
    super(c);
  }

  public MyRationalO8i(Connection c) throws SQLException  
  {
    super(c);
  }

  /* CustomDatum interface */
  /* toDatum() inherited from JPubRationalO8i */

  /* CustomDatumFactory interface */
  public CustomDatum create(Datum d, int sqlType) throws SQLException
  {
    if (d == null) return null;
    MyRationalO8i o = new MyRationalO8i();
    o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
    o._ctx = new _Ctx(((STRUCT) d).getConnection());
    return o;
  }

  /* accessor methods inherited from JPubRationalO8i */

  /* additional method not in base class */
  public String toString()
  {
    try
    {
       return getNumerator().toString() + "/" + getDenominator().toString();
    }
    catch (SQLException e)
    {
       return null;
    }
  }
}
