set echo on;

drop type BOOLEANS force;

CREATE TYPE BOOLEANS AS OBJECT (
  iIn    integer,
  iInOut integer,
  iOut   integer,

  member procedure p(i1 in boolean, i2 in out boolean, i3 out boolean),
  member function  f(i1 in boolean) return boolean
);
/

CREATE TYPE BODY BOOLEANS AS
MEMBER PROCEDURE p(i1 IN BOOLEAN, i2 IN OUT BOOLEAN, i3 OUT BOOLEAN) IS
BEGIN iOut := iIn;
      if    iInOut IS NULL THEN iInOut :=0;
      elsif iInOut=0 THEN iInOut :=1; 
      else  iInOut :=null;
      end if;
      i3 := i1; i2 := NOT i2;
END;

MEMBER FUNCTION f(i1 IN BOOLEAN) RETURN BOOLEAN IS
BEGIN return i1 = (iIn=1); end;

END;
/

commit;
exit
