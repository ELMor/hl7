import java.lang.reflect.Array;
import java.rmi.RemoteException;
import javax.ejb.*;
import javax.naming.*;
import javax.rmi.PortableRemoteObject;
import java.io.*;
import java.util.*;

/**
 * A simple client for accessing a BMP.
 */
public class BMPClient extends Thread
{
  public static void main(String[] args)
  {
    try
    {
      /**
       * Create access to the naming context.
       */
      Context context = new InitialContext();

      /**
       * Lookup the BMPHome object. The reference is retrieved from the
       * application-local context (java:comp/env). The variable is
       * specified in the assembly descriptor (META-INF/application-client.xml).
       */
      Object homeObject = context.lookup("java:comp/env/BMPEJB");

      // Narrow the reference to a BMPHome.
      BMPHome home = (BMPHome)PortableRemoteObject.narrow(homeObject, BMPHome.class);
      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

      System.err.println("*** Test Various Data Sources With EJB Creation, Store, Load ect ***");

      // Create new BMPs and narrow the reference.
      String name = "bmp1";
      float quantity = 200;
      System.err.println("****** Create EJB <" + name + ", " + quantity + ">");
      BMP bmp = (BMP)PortableRemoteObject.narrow(home.create(name, new Float(quantity)), BMP.class);

      name = "bmp2";
      quantity = 400;
      System.err.println("****** Create EJB <" + name + ", " + quantity + ">");
      bmp = (BMP)PortableRemoteObject.narrow(home.create(name, new Float(quantity)), BMP.class);

      name = "bmp3";
      quantity = 600;
      System.err.println("****** Create EJB <" + name + ", " + quantity + ">");
      bmp = (BMP)PortableRemoteObject.narrow(home.create(name, new Float(quantity)), BMP.class);

      name = "bmp4";
      quantity = 800;
      System.err.println("****** Create EJB <" + name + ", " + quantity + ">");
      bmp = (BMP)PortableRemoteObject.narrow(home.create(name, new Float(quantity)), BMP.class);

      System.err.println("****** Find All EJBs with SQLJ Data Sources");
      Collection recs = home.findAll();
      Iterator iterator = recs.iterator();
      int i = 0;
      while(iterator.hasNext()) {
            BMP rec =
              (BMP) PortableRemoteObject.narrow(iterator.next(),
                                                     BMP.class);
            System.out.println("row-" + i++ + " " + rec.getId()
                               + " " + rec.getName()
                               + " " + rec.getQuantity());
      }
      System.out.println("****** BMP.findAll(): found " + recs.size() + " BMPs");
      System.err.println("*** End Test Various Data Sources With EJB Creation, Store, Load ect ***");

      for (i=0; i<Array.getLength(args); i++)
      { 
        if (args[i].equalsIgnoreCase("failover")) {
          System.err.println("*** Start Failover Test  ***");
          System.err.println("Look up a datasource");
	  System.err.println(bmp.failover1());
          System.err.println("Kill the server that handles the last request");
          System.err.println("Service will be taken over by another server");
          System.err.println("Press any key to continue...");
	  System.in.read();
	  System.err.println(bmp.failover2());
          System.err.println("*** End Failover Test  ***");

        }    
        else if (args[i].equalsIgnoreCase("serTest")) {
          System.err.println("*** Serialization Test  ***");
	  System.err.println(bmp.serTest());
          System.err.println("*** End Serialization Test  ***");
        }    
      }
    }
    catch(NumberFormatException e)
    {
      System.err.println("Invalid number specified");
    }
    catch(RemoteException e)
    {
      System.err.println("System/communication error: " + e.getMessage());
    }
    catch(IOException e)
    {
      System.err.println("IO Error: " + e.getMessage());
    }
    catch(NamingException e)
    {
      System.err.println("Communication error: " + e.getMessage());
    }
    catch(CreateException e)
    {
      System.err.println("Error creating BMPs: " + e.getMessage());
    }
    catch(FinderException e)
    {
      System.err.println("Error BMP.findAll(): " + e.getMessage());
    }
  }
}
