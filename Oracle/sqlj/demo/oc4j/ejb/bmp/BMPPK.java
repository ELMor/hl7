// BMPPK.java

public class BMPPK implements java.io.Serializable
{
  public Integer id;
  public String name;
  public Float quantity;

  public BMPPK()
  {
    this.id = null;
    this.name = null;
    this.quantity = null;
  }
	
  public BMPPK(Integer id)
  {
    this.id = id;
    this.name = null;
    this.quantity = null;
  }

  public BMPPK(Integer id, String name, Float quantity)
  {
    this.id = id;
    this.name = name;
    this.quantity = quantity;
  }
  
}
