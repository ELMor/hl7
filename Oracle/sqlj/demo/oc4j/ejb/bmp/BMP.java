import java.rmi.RemoteException;
import javax.ejb.*;

public interface BMP extends EJBObject
{
	public int getId() throws RemoteException;

	public String getName() throws RemoteException;

	public void setName(String value) throws RemoteException;

	public float getQuantity() throws RemoteException;

	public void setQuantity(float value) throws RemoteException;

	public String failover1() throws RemoteException;

	public String failover2() throws RemoteException;

	public String serTest() throws RemoteException;
}
