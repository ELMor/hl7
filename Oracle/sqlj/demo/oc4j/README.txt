The directory contains two J2EE applications that demonstrate the usage of SQLJ in OC4J. The applications can be run under an Oracle iAS version 2.0 or later release. However, they cannot be run under the Oracle RDBMS product, due to lack of OC4J.  

The application contains a bean-managed persistent entity bean (the "ejb/bmp" directory). The application contains a web component (the "web" subdirectory). The usage of SQLJ-specific data sources are demonstrated through the beans in the applications, and SQLJ connection beans through the jsp programs (*.jsp and *.sqljsp) in the web components.  

This demo can be run with the $ORACLE_HOME/sqlj/demo/oc4j/bmp.ear file, using the deployment tools Oracle provides. However, one needs to follow steps (3) and (4) to set up data sources correctly. The following discussion describes the demo process step-by-step. 

Prepare for the Demo
====================

(1) This demo requires a running Oracle database, where the schema "scott/tiger" with the "emp" table created. The "emp" table can usually be found in Oracle RDBMS instance as demo tables. One can also run $ORACLE_HOME/bin/demobld to create that table.

(2) Make sure the following enviroment variables are set correctly:

	ORACLE_HOME, the root directory for ORACLE installation
	JAVA_HOME, the directory for JDK
	J2EE_HOME, the directory for OC4J home 

For instance, on Solaris, the following commands set up JAVA_HOME and J2EE_HOME:

%setenv JAVA_HOME /usr/local/packages/jdk1.3.1
%setenv J2EE_HOME ${HOME}/j2ee/home

(3) From data-sources.xml.prot to data-sources.xml: 
Based on the host name, listening port and SID of the running database, replace all the occurrances of the string

	%CONNSTRING%

file by <host>:<port>:<SID>, e.g., 

	localhost:1521:orcl

in the "demo/oc4j/etc/data-sources.xml.prot" file to form a new file "demo/oc4j/etc/data-sources.xml". Here is the "sed" command that does the job:

	sed -e 's/%CONNSTRING%/localhost:1521:orcl/g' <etc/data-sources.xml.prot>etc/data-sources.xml
	
(4) Copy etc/data-sources.xml to $J2EE_HOME/config/data-sources.xml. Alternatively, add the following entries into the $J2EE_HOME/config/data-sources.xml file:

<!-- SQLJ-Specific Data Sources-->
<data-source
	class="oracle.sqlj.runtime.OracleSqljDataSource"
	name="sqljtest/sqlj/OracleDS"
	location="sqljtest/sqlj/OracleDS/pool"
	xa-location="sqljtest/sqlj/OracleDS/xs"
	ejb-location="sqljtest/sqlj/OracleDS/ejb"
	pool-location="sqljtest/sqlj/OracleDS/pool"
	url="jdbc:oracle:thin:@localhost:1521:orcl"
	username="scott"
	password="tiger"
/>

<!-- Using OraclePooledConnection -->
<data-source
	class="oracle.sqlj.runtime.OracleSqljConnectionPoolDataSource"
	name="sqljtest/sqlj/OracleConnectionPoolDS"
	location="sqljtest/sqlj/OracleConnectionPoolDS"
	username="scott"
	password="tiger"
	url="jdbc:oracle:thin:@localhost:1521:orcl"
	inactivity-timeout="30"
/>

<!-- Using OracleConnectionCache-->
<data-source
	class="oracle.sqlj.runtime.OracleSqljConnectionCacheImpl"
	name="sqljtest/sqlj/OracleConnectionCacheImpl"
	location="sqljtest/sqlj/OracleConnectionCacheImpl"
	url="jdbc:oracle:thin:@localhost:1521:orcl"
	username="scott"
	password="tiger"
	max-connections ="30"
	min-connections ="5"
	inactivity-timeout="30"
/>

(5) Create the following directories:

	mkdir lib
	mkdir lib/bmp
	mkdir build
	mkdir build/bmp
	mkdir build/bmp/META-INF
	mkdir build/bmp/bmp-client
	mkdir build/bmp/bmp-client/META-INF
	mkdir build/bmp/bmp-ejb
	mkdir build/bmp/bmp-ejb/META-INF
	mkdir build/bmp/bmp-web
	mkdir build/bmp/bmp-web/META-INF
	mkdir build/bmp/bmp-web/WEB-INF

Run the Demo
============

(6) Modify the URL setting in the first line of the Makefile 
 
    URL=localhost:1521:orcl
 
according to the real database configuration. 

(7) Start the OC4J server
% make demo-server

(8) Build the "bmp" application:
% make demo-build

Compile the client and ejb directory and generate jar files for "bmp". Those jar files are bmp-ejb.jar, bmp-client.jar. Also generated is the ear file for the application. Those jar and ear files are put in the lib directory.  

(9) Deploy the "bmp" application: 
% make demo-deploy

(10) Run "bmp" clients:
% make demo-client

The client asks the server to create four new BMP beans. Various Sqlj-specifc data sources are employed to create a bean. After creating those beans, the client querys all the beans, which returns a list of beans, each mapped to a record in the emp table.

Expected output in the client side: 
===================================

*** Test Various Data Sources With EJB Creation, Store, Load ect ***
****** Create EJB <bmp1, 200.0>
****** Create EJB <bmp2, 400.0>
****** Create EJB <bmp3, 600.0>
****** Create EJB <bmp4, 800.0>
****** Find All EJBs with SQLJ Data Sources
row-0 7369 SMITH 800.0
row-1 7499 ALLEN 1600.0
row-2 7521 WARD 1250.0
row-3 7566 JONES 2975.0
row-4 7654 MARTIN 1250.0
row-5 7698 BLAKE 2850.0
row-6 7782 CLARK 2450.0
row-7 7788 SCOTT 3000.0
row-8 7839 KING 5000.0
row-9 7844 TURNER 1500.0
row-10 7876 ADAMS 1100.0
row-11 7900 JAMES 950.0
row-12 7902 FORD 3000.0
row-13 7934 MILLER 1300.0
row-0 131 bmp3 600.0
row-1 132 bmp4 800.0
row-2 129 bmp1 200.0
row-3 130 bmp2 400.0
****** BMP.findAll(): found 4 BMPs
*** End Test Various Data Sources With EJB Creation, Store, Load ect ***


Test JSPs:
=========

(11) Modify the following files, replacing localhost:1521:orcl using <host>:<port>:<SID> that reflects the database actually running:

	web/SqljConnBeanDemo.jsp
	web/SqljConnCacheBeanDemo.jsp 
	web/SqljCursorBeanDemo.jsp
	web/SqljIterator.sqljsp
	web/SqljSelectInto.sqljsp

Based on the host name, listening port and SID of the running database, replace all the occurrances of the string

        localhost:1521:orcl	

file by <host>:<port>:<SID>. 

(12) In a web browser, use the following URLs to access the JSPs applications: 

	http://localhost:XXXX/bmp/SqljConnBeanDemo.jsp
	http://localhost:XXXX/bmp/SqljConnCacheBeanDemo.jsp 
	http://localhost:XXXX/bmp/SqljCursorBeanDemo.jsp
	http://localhost:XXXX/bmp/SqljIterator.sqljsp
	http://localhost:XXXX/bmp/SqljSelectInto.sqljsp

where XXXX is the port number defined in default-web-site.xml, for instance, 7777. Note that if the deployment step deploys the web applcation in web-site rather than default-web-site, the port number need to be found in the xml file defined for that web-site.

Alternatively, clicking a link invokes a particular JSP applicaiton at the following URL:
	http://localhost:XXXX/bmp/


Distributibility:
================

OracleSqljDataSource (oracle.sqlj.runtime.OracleSqljDataSource) and SqljConnBean (oracle.sqlj.runtime.SqljConnBean) are serializable, thus can be used in clustering environment. Tests that use other data sources or SqljConnCacheBean (oracle.sqlj.runtime.SqljConnCacheBean) may produce NotSerializable exceptions when running in a clustered environment. 
