#
# Sample script for client translate + server load, run
#
# Environment setup:
# - have sqlj and javac in your PATH
# - SQLJ and JDBC libraries in your CLASSPATH
#
# In the demo directory, execute the following commands
#

# First, translate the sqlj file
sqlj ServerDemo.sqlj

# jar the generated files
jar cvf0 demo.jar ServerDemo*.class ServerDemo*.ser

# Clean up the database 
sqlplus scott/tiger @Drop.sql

# Load the jar into the server.
loadjava -oci8 -resolve -force -user scott/tiger demo.jar

# Publish SQL wrapper and run the SQLJ demo on the server
sqlplus scott/tiger @Run.sql

# In sqlplus, it should print 
# Hello! I'm SQLJ in server!
# Today is <date>
