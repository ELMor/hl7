
-- Drop the wrappers

drop procedure JAVAP;
/

drop procedure JAVA;
/
drop procedure JAVA1;
/
drop procedure JAVA2;
/
drop procedure JAVA3;
/
drop procedure JAVA4;
/
drop procedure JAVA5;
/
drop procedure JAVA6;
/
drop procedure JAVA7;
/
drop procedure JAVA8;
/
drop procedure JAVA9;
/

drop procedure JAVAI;
/
drop procedure JAVAFN;
/

-- Try to drop source for the examples if they were loaded as such
drop java source "Java";
/
drop java source "Javai";
/
drop java source "Javap";
/
drop java source "SqlWrappers";
/

-- Try to drop class files for the examples if they were loaded as such
drop java class "Java";
/
drop java class "Javai$JObject";
/
drop java class "Javai.JObject";
/
drop java class "Javai$Token";
/
drop java class "Javai.Token";
/
drop java class "Javai";
/
drop java class "Javap";
/
drop java class "SqlWrappers";
/
