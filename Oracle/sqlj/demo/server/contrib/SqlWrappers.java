import java.lang.reflect.*;
import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;

public class SqlWrappers
{
  public static void main(String[] args)
  {
    System.exit(statusMain(args));
  }

  public static int statusMain(String[] args)
  {
 
    m_package = null;
    m_main    = null;

      
    Vector v = new Vector();
    for (int i=0; i<args.length; i++)
    {
      if (args[i].startsWith("-"))
      {
        if (args[i].equals("-help"))
        {
           printHelp();
           return 0;
        }
        else if (args[i].equals("-package"))
        {
          m_package = "";
        }
        else if (args[i].startsWith("-package="))
        {
          m_package = args[i].substring(9);
        }
        else if (args[i].equals("-main"))
        {
          m_main = new boolean[256];
          m_main[0] = true;
        }
        else if (args[i].startsWith("-main="))
        {
          m_main = new boolean[256];

          try
          {
             String m = args[i].substring(6);
             StringTokenizer st = new StringTokenizer(m,",");
             while (st.hasMoreElements())
             {
               String s = st.nextToken();
               int idx = s.indexOf("-");
               if (idx>0)
               {
                 String t = s.substring(idx+1);
                 s = s.substring(0,idx);
                 int start = Integer.parseInt(s);
                 int end = Integer.parseInt(t);
   
                 for (int j=start; j<=end; j++)
                 {
                   m_main[j] = true;
                 }
               }
               else
               {
                 int id = Integer.parseInt(s);
                 m_main[id] = true; 
               }
             }
          }
          catch (Exception exn)
          {
             System.out.println("Error: illegal range in -main option.");
             printHelp();
             return 1;
          }
        }
        else
        {
          System.out.println("Error: invalid option: "+args[i]);
          printHelp();
          return 0;
        }
      }
      else
      {
        v.addElement(args[i]);
      }
    }

    args = new String[v.size()];

    for (int i=0; i<args.length; i++)
    {
      args[i] = (String) v.elementAt(i);
    }

    if (args.length==0) 
    {
      printHelp();
      return 1;
    }


    m_maps = new Hashtable();
    init_typemap();

    StringBuffer sb = new StringBuffer();

    sb.append(getAllSpecPrelude());
    for (int i=0; i<args.length; i++)
    {
      Class c = null;
      try
      {
        c = Class.forName(args[i]);
        sb.append(map(c, false));
      }
      catch (ClassNotFoundException exn)
      {
        System.out.println("Error: unknown class: "+args[i]);
        return 1;
      }
      catch (Exception exn)
      {
        System.out.println("Error: unexpected exception: "+exn);
        exn.printStackTrace();
        return 1;
      }
    }
    sb.append(getAllPostlude());

    if (m_package!=null && !m_package.equals(""))
    {
      sb.append("\n\n");
    }

    sb.append(getAllBodyPrelude());
    for (int i=0; i<args.length; i++)
    {
      Class c = null;
      try
      {
        c = Class.forName(args[i]);
        sb.append(map(c, true));
      }
      catch (ClassNotFoundException exn)
      {
        System.out.println("Error: unknown class: "+args[i]);
        return 1;
      }
      catch (Exception exn)
      {
        System.out.println("Error: unexpected exception: "+exn);
        exn.printStackTrace();
        return 1;
      }
    }
    sb.append(getAllPostlude());

    System.out.println(sb.toString());

    return 0;
  }
  private static Hashtable m_maps;
  private static String m_package;
  private static String m_current_package;
  private static boolean[] m_main;

  private static void printHelp()
  {
    System.out.println("SqlWrapper: creates SQL wrappers for Java classes.");
    System.out.println("Syntax: java SqlWrapper [options] classname [... classname]");
    System.out.println("where options is:");
    System.out.println("  -package         - generate wrappers in packages (package name derived from classname");
    System.out.println("  -package=pkgname - generate all wrapper into the package pkgname");
    System.out.println("          by default wrappers are created at the SQL top level");
    System.out.println("  -main            - generate a 0-argument wrapper for main");
    System.out.println("  -main=0,3-5,10   - generate wrappers for main with 0,3,4,5,10 arguments");
    System.out.println("          by default, no wrappers are created for main");
  }

  private static String map(Class c, boolean isBody) 
  {

    if (c.isArray())
    {
      System.out.println("Error: cannot map array classes: "+c.getName());
      System.exit(1);
    }

    if (!isBody && m_package ==null)
    {
      return "";
    }

    StringBuffer sb = new StringBuffer();

    m_current_package = c.getName();
    int idx = m_current_package.lastIndexOf(".");
    m_current_package = m_current_package.substring(idx+1);

    Method[] methods = c.getMethods();

    Vector sp_methods = new Vector();

    for (int i=0; i<methods.length; i++)
    {
      int modifier = methods[i].getModifiers();

      if (Modifier.isPublic(modifier) && Modifier.isStatic(modifier))
      {
        boolean mappable = true;

        Class rtype = methods[i].getReturnType();

        if (rtype!=void.class && !isMappable(rtype, true))
        {
          mappable = false;
          if (!isBody)
          {
            System.out.println("REM "+c.getName()+"."+methods[i].getName()+" returns "+
                               "unsupported type: "+rtype.getName());
          }
        }

        Class[] params = methods[i].getParameterTypes();

        for (int j=0; j<params.length; j++)
        {
           if (!isMappable(params[j], false))
           {
             mappable = false;

             if (!isBody)
             {
               System.out.println("REM "+c.getName()+"."+methods[i].getName()+
                              " unsupported type in arg"+(j+1)+": "+params[j].getName());
               break;
             }
           }
        }

        if (mappable)
        {
          sp_methods.addElement(methods[i]);
        }
      }
    }


    if (isBody)
    {
       sb.append(getClassBodyPrelude());
    }
    else
    {
       sb.append(getClassSpecPrelude());
    }

    for (int i=0; i<sp_methods.size(); i++)
    {
      sb.append(mapMethod(c, (Method) sp_methods.elementAt(i), isBody));
    }

    sb.append(getClassPostlude());

    return sb.toString();
  }

  private static String getAllSpecPrelude()
  {
    if (m_package !=null && !m_package.equals(""))
    {
      return "CREATE OR REPLACE PACKAGE "+m_package+" AS\n";
    } 
    else return "";
  }

  private static String getAllBodyPrelude()
  {
    if (m_package !=null && !m_package.equals(""))
    {
      return "CREATE OR REPLACE PACKAGE BODY "+m_package+" AS\n\n";
    } 
    else return "";
  }

  private static String getAllPostlude()
  {
    if (m_package !=null && !m_package.equals(""))
    {
      return "END "+m_package+";\n/\n";
    } 
    else return "";
  }

  private static String getClassSpecPrelude()
  {
    if (m_package !=null && m_package.equals(""))
    {
      return "CREATE OR REPLACE PACKAGE "+m_current_package+" AS\n";
    } 
    else return "";
  }

  private static String getClassBodyPrelude()
  {
    if (m_package !=null && m_package.equals(""))
    {
      return "CREATE OR REPLACE PACKAGE BODY "+m_current_package+" AS\n\n";
    } 
    else return "";
  }

  private static String getClassPostlude()
  {
    if (m_package !=null && m_package.equals(""))
    {
      return "END "+m_current_package+";\n/\n";
    } 
    else return "";
  }


  private static String mapMethod(Class c, Method m, boolean isBody)
  {

    if (!isBody && m_package==null) return "";

    StringBuffer sb = new StringBuffer();

    boolean isProcedure = m.getReturnType() == void.class;

    String sqlName = m.getName();
    Class[] params = m.getParameterTypes();
   

    if (sqlName.equals("main")
        && params.length==1
        && params[0].isArray()
        && params[0].getComponentType() == String.class)
    {
       if (m_main==null) return "";

       for (int nargs=0; nargs<m_main.length; nargs++)
       {
         if (m_main[nargs])
         {
            if (m_package==null)
            {
               sb.append("CREATE OR REPLACE ");
            }
            else
            {
               sb.append("   ");
            }
            sb.append("PROCEDURE " + sqlName);

            if (nargs > 0)
            {
              sb.append( "(" );
              for (int i=0; i<nargs-1; i++)
              {
                sb.append( "arg"+i+" VARCHAR2, ");
              }
              sb.append( "arg"+nargs+" VARCHAR2)");
           }

           if (!isBody)
           {
             sb.append( ";\n" );
           }
           else
           {
              sb.append( " AS LANGUAGE JAVA\n");
              sb.append( "   NAME '" + c.getName()+".main(java.lang.String[])';\n\n");
           }

           // Unable to overload main at the top level
           if (m_package==null)
           {
             return sb.toString();
           }
         }
       }

       return sb.toString();
    }
    else
    {
       if (m_package==null)
       {
         sb.append("CREATE OR REPLACE ");
       }
       else
       {
         sb.append("   ");
       }
   
       sb.append((isProcedure ? "PROCEDURE" : "FUNCTION") + " " + sqlName);
   
       if (params.length > 0)
       {
         sb.append( "(" );
   
         for (int i=0; i<params.length-1; i++)
         {
           sb.append( "arg"+i+" " + getMap(params[i], false)+", " );
         }
   
         int i = params.length-1;
         sb.append( "arg"+i+" " + getMap(params[i], false) );
   
         sb.append( ")" );
       }
   
       if (!isProcedure)
       {
         sb.append( " RETURN " + getMap(m.getReturnType(), true) );
       }
   
       if (!isBody)
       {
         sb.append( ";\n" );
   
         return sb.toString();
       }
       
   
       sb.append( " AS LANGUAGE JAVA\n");
   
       sb.append( "   NAME '" + c.getName()+"."+m.getName()+ "(");
   
       if (params.length > 0)
       {
         for (int i=0; i<params.length-1; i++)
         {
           sb.append( getType(params[i]) + "," );
         }
   
         int i = params.length-1;
         sb.append( getType(params[i]) );
       }
   
       sb.append( ")" );
   
       if (!isProcedure)
       {
         sb.append(" return "+ getType(m.getReturnType()) );
       }
   
       sb.append( "';\n\n" );
    }

    return sb.toString();
  }

  private static boolean isMappable(Class c, boolean isReturn)
  {
    if (m_maps==null) { init_typemap(); }

    if (!isReturn && c.isArray())
    {
      c = c.getComponentType();
    }

    return (m_maps.get(c) != null);
  }

  private static String getMap(Class c, boolean isReturn)
  {
    if (m_maps==null) { init_typemap(); }
    
    boolean isInOut = false;

    if (!isReturn && c.isArray())
    {
      isInOut = true;
      c = c.getComponentType();
    }

    return (isInOut ? "IN OUT ": "")+ m_maps.get(c);
  }

  private static String getType(Class c)
  {
    String typ = "";

    while (c.isArray())
    {
      typ = typ + "[]";
      c = c.getComponentType();
    }

    return c.getName() + typ;
  }

  private static void init_typemap()
  {
     m_maps.put(long.class,    "NUMBER"); 
     m_maps.put(Long.class,    "NUMBER"); 
     m_maps.put(int.class,     "NUMBER"); 
     m_maps.put(Integer.class, "NUMBER"); 
     m_maps.put(short.class,   "NUMBER"); 
     m_maps.put(Short.class,   "NUMBER"); 
     m_maps.put(byte.class,    "NUMBER"); 
     m_maps.put(Byte.class,    "NUMBER"); 
     m_maps.put(float.class,   "NUMBER"); 
     m_maps.put(Float.class,   "NUMBER"); 
     m_maps.put(double.class,  "NUMBER"); 
     m_maps.put(Double.class,  "NUMBER"); 
     m_maps.put(boolean.class, "NUMBER"); 
     m_maps.put(Boolean.class, "NUMBER"); 
     m_maps.put(oracle.sql.NUMBER.class, "NUMBER"); 
     m_maps.put(java.math.BigDecimal.class, "NUMBER"); 

     m_maps.put(oracle.sql.DATE.class,  "DATE"); 
     m_maps.put(java.sql.Date.class,  "DATE"); 
     m_maps.put(java.sql.Time.class,  "DATE"); 
     m_maps.put(java.sql.Timestamp.class,  "DATE"); 

     m_maps.put(oracle.sql.RAW.class,  "RAW"); 
     m_maps.put(byte[].class,  "RAW"); 

     m_maps.put(oracle.sql.ROWID.class,  "ROWID"); 

     m_maps.put(oracle.sql.BFILE.class,  "BFILE"); 

     m_maps.put(oracle.sql.BLOB.class,  "BLOB"); 

     m_maps.put(oracle.sql.CLOB.class,  "CLOB"); 

     m_maps.put(String.class,  "VARCHAR2"); 
     m_maps.put(oracle.sql.CHAR.class,  "VARCHAR2"); 
  } 
}
