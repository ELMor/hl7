import java.io.*;
import java.sql.*;
import java.util.Properties;
import java.util.Hashtable;
   
public class SqlBatch
{
   public static String URL      = "jdbc:oracle:oci8:@";
   public static String USER     = "scott";
   public static String PASSWORD = "tiger";

   private static int num_statements;
   private static int num_errors;
   
   public static void main(String[] args) throws SQLException
   {
     loadProps(null);

     for (int i=0; i<args.length; i++)
     {
    
       if (!args[i].toUpperCase().endsWith(".SQL"))
       {
         args[i] = args[i] + ".sql";
       }

       {
         errorsSeen = new Hashtable();

         num_statements = 0;
         num_errors = 0;

         System.out.println("***** Executing SQL from "+args[i]+" *****");
         System.out.print  ("Connecting "+USER+" to "+URL+"...");
         System.out.flush();
         createConnection();

         Statement st=null;
         try
         {
            st = m_conn.createStatement();
            st.execute("delete from USER_ERRORS");
         }
         catch (SQLException e) { };

         if (st!=null) { st.close(); }
         System.out.println("Done.");

         try
         {
           openFile(args[i]);

            String s = readStatement();
            while (s!=null)
            {
              String S = s.trim().toUpperCase();

              if (S.equals(""))
              {
                // skip
              }
              else
              {
                 st = null;
                 try
                 {
                   boolean printOK = true;

                   num_statements++;
                   st = m_conn.createStatement();
   
                   if (S.startsWith("SELECT"))
                   {
                     printOK = false;
                     executeQuery(st,s);
                   }
                   else
                   {
                     executeStatement(st,s);
                   }
   
                   SQLWarning warn = st.getWarnings();
                   while (warn != null)
                   {
                     System.out.println("    Warning: "+warn);
                     warn = warn.getNextWarning();
                     printOK = false;
                   }

                   if (S.startsWith("CREATE")
                       || S.startsWith("BEGIN")
                       || S.startsWith("DECLARE"))
                   {
                     if (checkErrors()) printOK = false;
                   }
                   if (printOK)
                   {
                     System.out.println(m_ok);
                   }
                 }
                 catch (SQLException exn)
                 {
                   num_errors++;
                   System.out.print("Error: ");
                   System.out.println(exn);
                 }
                 finally
                 {
                   try
                   {
                     if (st!=null) st.close();
                   }
                   catch (SQLException exn) { }
                 }
              }
              s = readStatement();
            }
            closeFile();

            System.out.print  ( num_statements+" statement"+
                                ((num_statements==1) ? "" : "s")+".");
            if (num_errors>0)
            {
              System.out.print  ( " "+num_errors+" error"+
                                  ((num_errors==1) ? "" : "s")+".");
            }
            System.out.println();
         }
         catch (IOException exn)
         {
           System.out.println("Error in reading from file "+args[i]+":\n"+exn);
           closeFile();
         }
       }
     }
   }

   private static void createConnection() throws SQLException
   {
     if (m_conn!=null) return;

     DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
     m_conn = DriverManager.getConnection(URL, USER, PASSWORD);
   }

   private static void openFile(String file) throws IOException
   {
      m_is = new BufferedInputStream(new FileInputStream(file));
      m_line_number=0;
      m_file=file;
   } 
   
   private static void closeFile()
   {
     try
     {
      if (m_is!=null) m_is.close();
     }
     catch (Exception exn) { };
   } 

   private static boolean checkErrors()
   {
     Statement st = null;
     ResultSet rs = null;
     boolean haveErrors = false;

     try
     {
       st = m_conn.createStatement();

       rs = st.executeQuery("select NAME, TYPE, SEQUENCE, LINE, POSITION, TEXT from USER_ERRORS");

       while (rs.next())
       {
         String hash=rs.getString(1)+":"+rs.getString(2)+":"+rs.getInt(3);
         if (errorsSeen.get(hash)==null)
         {
           errorsSeen.put(hash,"");
           haveErrors = true;

           int line = rs.getInt(4);
           int col = rs.getInt(5);
           String mesg = rs.getString(6);
           System.out.println(m_file+":"+ (m_position + line - 1)+"."+col+":"+mesg);
         }
       }
     } catch (SQLException exn) { }
     finally
     {
       try {
        if (rs!=null) rs.close(); 
       } catch (SQLException exn) { };
       try {
        if (st!=null) rs.close(); 
       } catch (SQLException exn) { };
     }

     return haveErrors;
   }
   private static Hashtable errorsSeen = null;

   private static void executeStatement(Statement st, String s) throws SQLException
   { 
     st.executeUpdate(s);

     int count = st.getUpdateCount();
 
     m_ok = "  ok."+ ((count > 0) ? (" "+count+" row"+(count>1 ? "s" : "") +" updated.") : "");
   }
   private static String m_ok;

   private static void executeQuery(Statement st, String s) throws SQLException
   { 
     ResultSet rs = st.executeQuery(s);
     int count = 0;

     ResultSetMetaData rsm = rs.getMetaData();

     int numCols = rsm.getColumnCount();

     for (int i=1; i<= numCols; i++)
     {
       System.out.print(  "Column "+i+": "+
                          rsm.getColumnLabel(i)+" "+
                          rsm.getColumnTypeName(i));
       int prec  = rsm.getPrecision(i);
       int scale = rsm.getScale(i);

       if (scale == 0)
       {
         if (prec == 0)
         {

         }
         else
         {
           System.out.print("("+prec+")");
         }
       }
       else
       {
         System.out.print("("+prec+","+scale+")");
       }

       System.out.println();
     }

     while (rs.next())
     {
       count++;
       System.out.print("Row "+count+": ");
       for (int j=1; j<=numCols; j++)
       {
         System.out.print
          (stringValueOf
           (((oracle.jdbc.driver.OracleResultSet)rs).getOracleObject(j)));
         if (j<numCols) System.out.print(", ");
       }
       System.out.println();
     }
 
     System.out.println("  selected "+count+" row"+(count>1 ? "s" : "")+".");
   }

   private static String stringValueOf(oracle.sql.RAW raw) throws SQLException
   {
     if (raw==null) return "NULL";
     return raw.stringValue();
   }

   private static String stringValueOf(oracle.sql.ROWID rowid) throws SQLException
   {
     if (rowid==null) return "NULL";
     return rowid.stringValue();
   }

   private static String stringValueOf(oracle.sql.CHAR ch) throws SQLException
   {
     if (ch==null) return "NULL";
     return "'"+ch.stringValue()+"'";
   }

   private static String stringValueOf(oracle.sql.NUMBER num) throws SQLException
   {
     if (num==null) return "NULL";
     return num.stringValue();
   }

   private static String stringValueOf(oracle.sql.DATE date) throws SQLException
   {
     if (date==null) return "NULL";
     return date.stringValue();
   }

   private static String stringValueOf(oracle.sql.STRUCT struct) throws SQLException
   {
     if (struct==null) return "NULL";

     StringBuffer sb = new StringBuffer(struct.getSQLTypeName());
     sb.append("(");

     oracle.sql.Datum[] attr = struct.getOracleAttributes();
     for (int i=0; i<attr.length; i++)
     {
       sb.append(stringValueOf(attr[i]));
       if (i<attr.length-1)
       {
         sb.append(",");
       }
     }
     sb.append(")");
     return sb.toString();
   }

   private static String stringValueOf(oracle.sql.REF ref) throws SQLException
   {
     if (ref==null) return "NULL";

     return "REF "+ref.getBaseTypeName()+"{"+ref+"}";
   }

   private static String stringValueOf(oracle.sql.ARRAY arr) throws SQLException
   {
     if (arr==null) return "NULL";
     StringBuffer sb = new StringBuffer(arr.getSQLTypeName());
     sb.append("[");

     oracle.sql.Datum[] attr = arr.getOracleArray();
     for (int i=0; i<attr.length; i++)
     {
       sb.append(stringValueOf(attr[i]));
       if (i<attr.length-1)
       {
         sb.append(",");
       }
     }
     sb.append("]");
     return sb.toString();
   }

   private static String stringValueOf(oracle.sql.BLOB blob) throws SQLException
   {
     if (blob==null) return "NULL";
     return "BLOB{"+stringValueOf(blob.getLocator())+"}";
   }

   private static String stringValueOf(oracle.sql.CLOB clob) throws SQLException
   {
     if (clob==null) return "NULL";
     return "CLOB{"+stringValueOf(clob.getLocator())+"}";
   }

   private static String stringValueOf(oracle.sql.BFILE bfile) throws SQLException
   {
     if (bfile==null) return "NULL";
     return "BFILE{"+stringValueOf(bfile.getLocator())+"}";
   }

   private static int MAX_BYTE_PRINT = 10;
   private static String stringValueOf(byte[] barr) throws SQLException
   {
     StringBuffer sb = new StringBuffer();
     for (int i=0; i<barr.length && i < MAX_BYTE_PRINT; i++)
     {
       sb.append(hexValue(barr[i]));
     }
     if (MAX_BYTE_PRINT < barr.length) 
     {
       sb.append(".");
     }
     if (MAX_BYTE_PRINT+1 < barr.length) 
     {
       sb.append(".");
     }
     if (MAX_BYTE_PRINT+2 < barr.length) 
     {
       sb.append(".");
     }
     return sb.toString();
   }

   private static String hexValue(byte b)
   {
     int i = (int)b;
     if (i<0) i += 128;

     int hn = i / 16;
     int ln = i - 16 * hn;

     return hex[hn] + hex[ln];
   }
   private static String[] hex = new String[] 
    { "0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
     

   private static String stringValueOf(oracle.sql.Datum dat) throws SQLException
   {
     if (dat==null)
     {
       return "NULL";
     }
     else if (dat instanceof oracle.sql.BFILE)
     {
       return stringValueOf((oracle.sql.BFILE)dat);
     }
     else if (dat instanceof oracle.sql.CLOB)
     {
       return stringValueOf((oracle.sql.CLOB)dat);
     }
     else if (dat instanceof oracle.sql.BLOB)
     {
       return stringValueOf((oracle.sql.BLOB)dat);
     }
     else if (dat instanceof oracle.sql.NUMBER)
     {
       return stringValueOf((oracle.sql.NUMBER)dat);
     }
     else if (dat instanceof oracle.sql.REF)
     {
       return stringValueOf((oracle.sql.REF)dat);
     }
     else if (dat instanceof oracle.sql.STRUCT)
     {
       return stringValueOf((oracle.sql.STRUCT)dat);
     }
     else if (dat instanceof oracle.sql.ARRAY)
     {
       return stringValueOf((oracle.sql.ARRAY)dat);
     }
     else if (dat instanceof oracle.sql.DATE)
     {
       return stringValueOf((oracle.sql.DATE)dat);
     }
     else if (dat instanceof oracle.sql.RAW)
     {
       return stringValueOf((oracle.sql.RAW)dat);
     }
     else if (dat instanceof oracle.sql.CHAR)
     {
       return stringValueOf((oracle.sql.CHAR)dat);
     }
     else if (dat instanceof oracle.sql.ROWID)
     {
       return stringValueOf((oracle.sql.ROWID)dat);
     }
     else
     {
       return "<UNKNOWN: "+dat+">";
     }
   }



   private static int m_line_number;
   private static int m_position;
   private static String m_file;
   private static InputStream m_is = null;
   private static Connection m_conn = null;
   
   private static String getPosition()
   {
     return m_file+":"+m_line_number+": ";
   }
   
   private static String readStatement() throws IOException
   {
     boolean eof = true;
     StringBuffer sb=new StringBuffer();
     String s;

     m_position = m_line_number;

     while ((s=readLine()) != null && (!s.trim().equals("/")))
     {
       System.out.println(getPosition()+s);

       if (s.trim().toUpperCase().startsWith("CONNECT "))
       {
         System.out.println("Info: CONNECT statement ignored!");
         System.out.println();
       }
       else
       {
         if (!s.trim().equals(""))
         {
           sb.append(s); sb.append("\n");
         }
       }
       eof=false;
     }

     if (eof) return null;

     s = sb.toString().trim();

     if (!s.toUpperCase().startsWith("CREATE")
         && s.endsWith(";"))
     {
       s = s.substring(0,s.length()-1);
     }
     
     return s;
   }
   
   private static String readLine() throws IOException
   {
     StringBuffer sb=new StringBuffer();
     int ch;
     boolean eof = true;
   
     while ((ch=m_is.read()) > 0 && ch != (int)'\n')
     {
       sb.append((char)ch);
       eof = false;
     }

     if (eof && ch == (int) '\n')
     {
       eof = false;
     }

     m_line_number++;
   
     if (eof) return null;
   
     return sb.toString();
   }

  private static void loadProps(String propName)
  throws SQLException
  {
    InputStream ps = null;
    if (propName == null) {
      propName = "connect.properties";
    }
    if (propName.startsWith("/")) {
      propName = propName.substring(1);
    }
    
    try
    {
      ps = new BufferedInputStream(new FileInputStream(propName));
    }
    catch (IOException exn) { };

    if (ps == null) {
      System.err.println("Cannot load properties "+propName);
    }

    Properties props = new Properties();
    try {
      props.load(ps);
      ps.close();
    } catch (IOException e) {
      System.err.println("Cannot load properties "+propName);
    }

    URL = props.getProperty("sqlj.url", URL);
    USER = props.getProperty("sqlj.user", USER);
    PASSWORD = props.getProperty("sqlj.password", PASSWORD);
  }
}
