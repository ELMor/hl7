--------------------
--- Create JAVAP ---
--------------------

CREATE OR REPLACE PROCEDURE JAVAP(class VARCHAR2)
   AS LANGUAGE JAVA NAME 'Javap.main(java.lang.String[])';
/


---------------------------------------------------
--- Create JAVA and its variants JAVA1 .. JAVA9 ---
---------------------------------------------------

CREATE OR REPLACE PROCEDURE JAVA(class VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA1(class VARCHAR2, arg1 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA1(class VARCHAR2, arg1 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA2(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA3(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA4(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2, arg4 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA5(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2, arg4 VARCHAR2, arg5 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA6(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2, arg4 VARCHAR2, arg5 VARCHAR2,
                                   arg6 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA7(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2, arg4 VARCHAR2, arg5 VARCHAR2,
                                   arg6 VARCHAR2, arg7 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA8(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2, arg4 VARCHAR2, arg5 VARCHAR2,
                                   arg6 VARCHAR2, arg7 VARCHAR2, arg8 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/

CREATE OR REPLACE PROCEDURE JAVA9(class VARCHAR2, arg1 VARCHAR2, arg2 VARCHAR2,
                                   arg3 VARCHAR2, arg4 VARCHAR2, arg5 VARCHAR2,
                                   arg6 VARCHAR2, arg7 VARCHAR2, arg8 VARCHAR2,
                                   arg10 VARCHAR2)
   AS LANGUAGE JAVA NAME 'Java.main(java.lang.String[])';
/


-------------------------------
--- Create JAVAI and JAVAFN ---
-------------------------------

CREATE OR REPLACE PROCEDURE JAVAI(expr VARCHAR2) AS LANGUAGE JAVA
   NAME 'Javai.main(java.lang.String[])';
/

CREATE OR REPLACE FUNCTION JAVAFN(expr VARCHAR2) RETURN VARCHAR2 AS LANGUAGE JAVA
   NAME 'Javai.fun(java.lang.String) return java.lang.String';
/

CREATE OR REPLACE FUNCTION JAVAFN(expr VARCHAR2) RETURN VARCHAR2 AS LANGUAGE JAVA
   NAME 'Javai.fun(java.lang.String) return java.lang.String';
/
