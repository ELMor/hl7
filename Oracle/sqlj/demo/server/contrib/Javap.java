import java.util.Vector;
import java.util.StringTokenizer;
import java.lang.reflect.Modifier;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;

public class Javap
{

  private static final int PREFIX = 4;
  private static final int INDENT = 4;

  private static final int PRIVATE = 1;
  private static final int PACKAGE = 2;
  private static final int PROTECTED = 3;
  private static final int PUBLIC = 4;

  private static boolean _throws = false;
  private static boolean _value  = false;
  private static boolean _recurse  = false;
  private static int     _modifier = PACKAGE;

  private Javap() { };

 // class InnerClass { private int vi; int i; protected int oi; public int ui; }
 // public class InnerPublicClass {  private int pvi; int pi; protected int poi; public int pui; };
 // static class InnerStaticClass {  private int svi; int si; protected int soi; public int sui; };
 // private class InnerPrivateClass {  private int vvi; int vi; protected int voi; public int vui; };

  public static void main(String[] args)
  {
    // Reset settings in the server
    _throws = false;
    _value  = false;
    _recurse  = false;
    _modifier = PACKAGE;

    if (args.length==1)
    {
      if (args[0]==null || args[0].equals(""))
      {
        args = new String[]{};
      }
      else if (args[0].indexOf(" ") > 0)
      { StringTokenizer st = new StringTokenizer(args[0]);
        Vector v = new Vector();
        while (st.hasMoreElements())
        {
          v.addElement(st.nextElement());
        } 
        args = new String[v.size()];
        for (int i=0; i<v.size(); i++)
        {
          args[i]=(String)v.elementAt(i);
        }
      }
    }

    if (args.length==0) { usage(); return; }

    for (int i=0; i<args.length; i++) {
      if (args[i].startsWith("-")) {
         if (args[i].equalsIgnoreCase("-throws")) {
           _throws = true;
         } else if (args[i].equalsIgnoreCase("-nothrows")) {
           _throws = false;
         } else if (args[i].equalsIgnoreCase("-value")) {
           _value = true;
         } else if (args[i].equalsIgnoreCase("-novalue")) {
           _value = false;
         } else if (args[i].equalsIgnoreCase("-recurse")) {
           _recurse = true;
         } else if (args[i].equalsIgnoreCase("-norecurse")) {
           _recurse = false;
         } else if (args[i].equalsIgnoreCase("-public")) {
           _modifier = PUBLIC;
         } else if (args[i].equalsIgnoreCase("-protected")) {
           _modifier = PROTECTED;
         } else if (args[i].equalsIgnoreCase("-package")) {
           _modifier = PACKAGE;
         } else if (args[i].equalsIgnoreCase("-private")) {
           _modifier = PRIVATE;
         } else if (args[i].equalsIgnoreCase("-all")) {
           _throws = true; _value = true; _modifier = PRIVATE;
         } else {
           System.out.println("Error: Invalid option "+args[i]+".");
           usage();
         }
      }
      else
      {
        if (args[i].endsWith(".class"))
           args[i]=args[i].substring(0,args[i].length()-6);
        try {
          Class c = classForName(args[i]);
          javap(c, 0);
        }
        catch (Throwable t) {
          System.out.println("Error: "+t.toString());
        };
      }
    };
  }

  private static void javap(Class c, int nest)
  {
     printClassInfo(c, nest);
     System.out.println(prefix(nest) + "{");
     printFieldInfo(c, nest);
     printConstructorInfo(c, nest);
     printMethodsInfo(c, nest);
     printNestedClasses(c, nest);
     System.out.println(prefix(nest) + "}");
  }

  private static void usage()
  {
    System.out.println("Usage: Javap [-options] classname1 classname2 ...");
    System.out.println("Prints out information on one or more classes. Supported options are:");
    System.out.println(" -public    - print public members only");
    System.out.println(" -protected - print public and protected members");
    System.out.println(" -package   - print public, protected, and package-level members (Default)");
    System.out.println(" -private   - print all members");
    System.out.println(" -value     - print out value of public static final fields");
    System.out.println(" -throws    - print out throws information on methods");
    System.out.println(" -recurse   - print out inherited members");
    System.out.println(" -all       - synonymous with -private -throws -value");
  }

  private static void printClassInfo(Class c, int nest)
  {
    try {
      String m = Modifier.toString(c.getModifiers());
      if (!m.equals("")) m = m+" ";

      System.out.print(prefix(nest)+m+ ((c.isInterface()) ? "interface " : "class ") + c.getName());
      Class sup = c.getSuperclass();
      if (sup!=null) System.out.print(" extends "+printClass(sup));
      Class[] itf = c.getInterfaces();
      if (itf!=null && itf.length!=0) {
         System.out.print(" implements ");
         for (int i=0; i<itf.length-1; i++) {
             System.out.print(itf[i].getName()+", ");
         }
         System.out.print(itf[itf.length-1].getName());
      }
      System.out.println();
    }
    catch (Throwable t) {
       System.out.println("\nError: "+t.toString());
    }
  }

  private static void printNestedClasses(Class c, int nest)
  {
   try {
     Class[] cary = c.getDeclaredClasses();
     if (_recurse) {
        Class[] inh = c.getClasses();
        Object[] delta=merge(cary, inh);
        if (delta != null && delta.length > 0) {
           Class[] tmp=new Class[cary.length+delta.length];
           System.arraycopy(cary, 0, tmp, 0, cary.length);
           System.arraycopy(delta, 0, tmp, cary.length, delta.length);
           cary = tmp;
        }
     }

     Vector v = new Vector();
     for (int i=0; i<cary.length; i++) {
       if (accessible(cary[i].getModifiers())) v.addElement(cary[i]);
     }
     cary = new Class[v.size()];
     for (int i=0; i<v.size(); i++) cary[i] = (Class)v.elementAt(i);

     for (int i=0; i<cary.length; i++) {
       javap(cary[i], nest+INDENT);
     }
   } catch (Throwable t) {
     System.out.println("\nError: "+t.toString());
   }
  }

  private static void printFieldInfo(Class c, int nest) {
   try {
     Field[] fary = c.getDeclaredFields(); 
     if (_recurse) {
        Field[] inh = c.getFields();
        Object[] delta=merge(fary, inh);
        if (delta != null && delta.length > 0) {
           Field[] tmp=new Field[fary.length+delta.length];
           System.arraycopy(fary, 0, tmp, 0, fary.length);
           System.arraycopy(delta, 0, tmp, fary.length, delta.length);
           fary = tmp;
        }
     }

     Vector v = new Vector();
     for (int i=0; i<fary.length; i++) {
       if (accessible(fary[i].getModifiers())) {
          v.addElement(fary[i]);
       }
     }
     fary = new Field[v.size()];
     for (int i=0; i<v.size(); i++) fary[i] = (Field)v.elementAt(i);

     for (int i=0; i<fary.length; i++) {
       System.out.println(prefix(nest+PREFIX)+printField(fary[i])+";");
     }
   }
   catch (Throwable t) {
     System.out.println("\nError: "+t.toString());
   }
  }

  private static void printConstructorInfo(Class c, int nest) {
   try {
     Constructor[] cary = c.getDeclaredConstructors(); 
     if (_recurse) {
        Constructor[] inh = c.getConstructors();
        Object[] delta=merge(cary, inh);
        if (delta != null && delta.length > 0) {
           Constructor[] tmp=new Constructor[cary.length+delta.length];
           System.arraycopy(cary, 0, tmp, 0, cary.length);
           System.arraycopy(delta, 0, tmp, cary.length, delta.length);
           cary = tmp;
        }
     }

     Vector v = new Vector();
     for (int i=0; i<cary.length; i++) {
       if (accessible(cary[i].getModifiers())) {
          v.addElement(cary[i]);
       }
     }
     cary = new Constructor[v.size()];
     for (int i=0; i<v.size(); i++) cary[i] = (Constructor)v.elementAt(i);

     for (int i=0; i<cary.length; i++) {
       System.out.println(prefix(nest+PREFIX)+cary[i]+";");
     }
   }
   catch (Throwable t) {
     System.out.println("\nError: "+t.toString());
   }
  }

  private static void printMethodsInfo(Class c, int nest) {
   try {
     Method[] mary = c.getDeclaredMethods(); 
     if (_recurse) {
        Method[] inh = c.getMethods();
        Object[] delta=merge(mary, inh);
        if (delta != null && delta.length > 0) {
           Method[] tmp=new Method[mary.length+delta.length];
           System.arraycopy(mary, 0, tmp, 0, mary.length);
           System.arraycopy(delta, 0, tmp, mary.length, delta.length);
           mary = tmp;
        }
     }

     Vector v = new Vector();
     for (int i=0; i<mary.length; i++) {
       if (accessible(mary[i].getModifiers())) {
          v.addElement(mary[i]);
       }
     }
     mary = new Method[v.size()];
     for (int i=0; i<v.size(); i++) mary[i] = (Method)v.elementAt(i);

     for (int i=0; i<mary.length; i++) {
       System.out.println(prefix(nest+PREFIX)+printMethod(mary[i])+";");
     }
   }
   catch (Throwable t) {
     System.out.println("\nError: "+t.toString());
   }
 }

 private static String printMethod(Method m)
 {
   String res = Modifier.toString(m.getModifiers());
   if (!res.equals(""))
   {
     res = res + " ";
   }
   res = res + printClass(m.getReturnType())+" "+m.getName()+"("; //)

   Class[] cary=m.getParameterTypes();
   if (cary!=null && cary.length>0) {
    for (int i=0; i<cary.length-1; i++) {
      res = res + printClass(cary[i]) + ", ";
    }
    res = res + printClass(cary[cary.length-1]);
   }
   // (
   res = res + ")";

   if (_throws) {
     cary = m.getExceptionTypes();
     if (cary!=null && cary.length>0) {
      res = res + " throws ";
      for (int i=0; i<cary.length-1; i++) {
        res = res + printClass(cary[i]) + ", ";
      }
      res = res + printClass(cary[cary.length-1]);
     }
   }

   return res;
 }

 private static String printField(Field f)
 {
   int m = f.getModifiers();
   String res = Modifier.toString(m);

   if (!res.equals("")) res = res + " ";

   res = res + printClass(f.getType()) + " " + f.getName();

   if (_value && Modifier.isStatic(m) /* && Modifier.isPublic(m) && Modifier.isFinal(m) */ )
   {
     try
     {
       Object o = f.get(null);
       res = res + " = " + printObject(o);
     }
     catch (Throwable t) { };
   };

   return res;
 }

 private static String printClass(Class c)
 {
   String res = "";
   while (c.isArray()) {
     res = res + "[]";
     c = c.getComponentType();
   }
   return c.getName()+res;
 }

 private static boolean accessible(int m)
 {
   if (_modifier==PRIVATE) return true;
   if (_modifier==PACKAGE) return (!Modifier.isPrivate(m));
   if (_modifier==PROTECTED) return (Modifier.isProtected(m) || Modifier.isPublic(m));
   if (_modifier==PUBLIC) return Modifier.isPublic(m);

   throw new IllegalArgumentException("Modifier level "+_modifier+" unknown! Should not occur.");
 }

 private static String printObject(Object o)
 {
   if (o==null) return "null";

   if (o instanceof String)
   {
     return "\"" + escape((String)o) + "\""; 
   }
   else if (o instanceof Character)
   {
     char ch = ((Character)o).charValue();
     if (ch=='\'') return "'\\''";
     if (ch=='"') return "'\"'";

     return "'" + escape(((Character)o).toString()) + "'";
   }
   else if (o instanceof Class)
   {
     return printClass((Class)o) + ".class";
   }
   else if (o.getClass().isArray())
   {
     String res = "{";
     int len = Array.getLength(o);
     for (int i=0; i<len; i++)
     {
       res = res + printObject(Array.get(o,i));
       if (i<len-1) res = res + ",";
     }
     return res + "}";
   }
   else
   {
     return o.toString();
   }
 }

 private static String escape(String s)
 {
   StringBuffer sb = new StringBuffer();

   for (int i=0; i<s.length(); i++) {
     char ch = s.charAt(i);
     if (ch=='\n') {
       sb.append("\\n");
     } else if (ch=='\r') {
       sb.append("\\r");
     } else if (ch=='\t') {
       sb.append("\\t");
     } else if (ch=='"') {
       sb.append("\\\"");
     } else if (ch=='\\') {
       sb.append("\\\\");
     } else {
       sb.append(ch);
     }
  }
  return sb.toString();
 }

 private static Object[] merge(Object[] org, Object[] neu)
 {
   Vector v = new Vector();
   for (int i=0; i<neu.length; i++) {
     boolean found = false;
     for (int j=0; j<org.length; j++) {
       if (org[j]==neu[i]) { found=true; break; }
     }
     if (!found) v.addElement(neu[i]);
   }
   if (v.size()==0) return null;

   Object[] oa = new Object[v.size()];
   v.copyInto(oa);
   return oa;
 }

 private static String prefix(int i)
 {
   return BLANKS.substring(0,i);
 }
 private static final String BLANKS = "                                                                  ";


 private static Class classForName(String s) throws ClassNotFoundException
 {
   Class c = null;
   if (s==null /*|| s.equals("void")*/) return c;

   if (s.equals("void"))
   {
     c = void.class; 
   }
   else if (s.equals("boolean"))
   {
     c = boolean.class; 
   }
   else if (s.equals("byte"))
   {
     c = byte.class; 
   }
   else if (s.equals("short"))
   {
     c = short.class; 
   }
   else if (s.equals("char"))
   {
     c = char.class; 
   }
   else if (s.equals("int"))
   {
     c = int.class; 
   }
   else if (s.equals("long"))
   {
     c = long.class; 
   }
   else if (s.equals("float"))
   {
     c = float.class; 
   }
   else if (s.equals("double"))
   {
     c = double.class; 
   }
   else
   {
     ClassNotFoundException e = null;
     try
     {
       c = Class.forName(s);
     }
     catch (ClassNotFoundException exn)
     {
       e = exn;
       try {
         c = Class.forName("java.lang."+s);
       } catch (ClassNotFoundException exn2) {
         throw e;
       }
     }
   }

   return c;
 }

}
