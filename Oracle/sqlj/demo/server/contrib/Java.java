import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import java.util.Vector;
import java.util.StringTokenizer;

import java.sql.SQLException;

import oracle.sqlj.runtime.Oracle;


public class Java
{
  public static void main(String[] args)
  {
    if (args.length==1)
    {
      if (args[0]==null || args[0].equals(""))
      {
        args = new String[]{};
      }
      else if (args[0].indexOf(" ") > 0)
      { StringTokenizer st = new StringTokenizer(args[0]);
        Vector v = new Vector();
        while (st.hasMoreElements())
        {
          v.addElement(st.nextElement());
        } 
        args = new String[v.size()];
        for (int i=0; i<v.size(); i++)
        {
          args[i]=(String)v.elementAt(i);
        }
      }
    }

    if (args.length==0) {
       System.out.println("Usage: CALL JAVA('classname','arg1',..)");
    } else {
      if (!done) init();

      try 
      {
        Class c = Class.forName(args[0]);
        Method m = c.getMethod("main", STRINGARY_ARG);

        String[] p = new String[args.length-1];
        System.arraycopy(args,1,p,0,p.length);

        m.invoke(null, new Object[]{ p });
      }
      catch (InvocationTargetException ex)
      {
         Throwable e = ex.getTargetException();
         e.printStackTrace(System.out);
         System.out.println("Uncaught target exception: "+e);
      }
      catch (Throwable t)
      {
        // t.printStackTrace();
        System.out.println("Error: "+t);
      }
    } 
  }
  private static final Class[] STRINGARY_ARG = new Class[]{String[].class};

  private static boolean done = false;
  private static void init()
  {
    /*
    try
    {
      Oracle.connect("jdbc:oracle:thin:@erohwedd-pc:1521:orcl","scott","tiger");
      // #sql { set echo on };
      // #sql { set termout on };
      // #sql { set flush on };
      // #sql { set serveroutput on };
      #sql { call dbms_java.set_output(10000) };
    }
    catch (SQLException exn)
    {
      System.out.println("Error: "+exn.toString());
    } 
    */

    done = true;
  }
}
