Drop Trigger hl7_fusion_adta34;

--Notificacion al exterior de Fusion de Pacientes
Create Or Replace Trigger hl7_fusion_adta34
   Before Update
   On 		hc
   For Each Row
Declare
   nhc_id1  Number;
   nhc_id2  Number;
   codeve   Number;
   bldcmd   Varchar2 (512);
   cc       Number;
   ca       Number;
Begin
   If :OLD.codigo_cliente is null or
   		:NEW.codigo_cliente is null or
      :OLD.codigo_cliente = :NEW.codigo_cliente
   Then
      Return; --No Notificar
   End If;

   --Codigo de evento y funcion que obtiene el texto del mensaje
   Select hl01codevento, hl01bldfunc
     Into codeve, bldcmd
     From hl01
    Where hl01ident = 'ADT^A34';

   cc:= :NEW.codigo_cliente;
   ca:= :OLD.codigo_cliente;

   bldcmd    :=    bldcmd
                || '('
                || TO_CHAR (cc)
                || ',<hl02CodSys_des>,'
                || TO_CHAR (codeve)
                || ','
                || TO_CHAR (ca)
                || ')';
   hl7notif.todos (bldcmd, codeve, cc);
End;
/
